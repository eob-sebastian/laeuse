<form method="get" class="search-form" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
	<fieldset>
		<input type="search" class="search" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'mf' ); ?>" value="<?php echo esc_attr(get_search_query()); ?>" name="s" title="<?php echo esc_attr( 'Search for:', 'label', 'mf' ); ?>" />
		<button type="submit" class="submit">
        	<i class="fa fa-search"></i>
    	</button>
	</fieldset>
</form>