<?php
/**
* @package WordPress
* @subpackage Default_Theme
*/
// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
die ('Please do not load this page directly. Thanks!');
if ( post_password_required() ) { ?>
<p class="nocomments">This post is password protected. Enter the password to view comments.
</p>
<?php
return;
}
$GLOBALS['comment'] = $comment;
?>

<div id="comments" class="comments-area">

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<h4><?php echo esc_html__('Comments','mf') .' ('. wp_count_comments(get_the_ID())->total_comments .')'?></h4>
		<ul class="list-unstyled list">
			<?php
				wp_list_comments( array(
                    'avatar_size'   => 80,
                    'callback'      => 'mf_comments'
				) );
			?>
		</ul><!-- .comment-list -->
	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php esc_html__( 'Comments are closed.', 'mf' ); ?></p>
	<?php endif; ?>

	<?php
    $commenter = wp_get_current_commenter();
    $req = get_option( 'require_name__mail' );
    $aria_req = ( $req ? " aria-required='true'" : '' );
    $args = array(
            'id_form'           => 'commentform',
            'id_submit'         => 'submit',
			'title_reply_before'=> '<h4>',
			'title_reply_after'=> '</h4>',
            'title_reply'       => esc_html__( 'Leave Comment', 'mf' ),
            'title_reply_to'    => esc_html__( 'Leave A Reply To %s', 'mf' ),
            'cancel_reply_link' => esc_html__( 'Cancel Reply', 'mf' ),
            'label_submit'      => esc_html__( 'Submit', 'mf' ),
            'comment_notes_before' => '',
			'comment_notes_after' => '',
            'fields' => apply_filters( 'comment_form_default_fields', array(
                'author' =>
                    '<div class="form-row">' .
                            '<input id="author" class="input" name="author" type="text" aria-required="true" required="required" placeholder="' . esc_html__( 'Your Name *', 'mf' ) . '"/>' ,

                'email' =>
                        
                            '<input id="email" class="input" name="email" type="email" aria-describedby="email-notes" aria-required="true"' .
                                ' required="required" placeholder="' . esc_html__( 'Email*', 'mf' ) . '"/>'  ,

                'website' =>
                        
                            '<input id="url" class="input" name="url" type="url"
                             placeholder="' . esc_html__( 'Website', 'mf' ) . '"/>' .
                        
                    '</div>'
                )
            ),
            'comment_field' =>  
                '<textarea id="comment" name="comment" cols="30" rows="10"'.
                ' class="color_grey d_block r_corners w_full height_4"' .
                ' placeholder="'. esc_html__('Review *', 'mf' ).'" aria-required="true">' .
            '</textarea>',
    );
    comment_form($args);?>

</div><!-- #comments -->
