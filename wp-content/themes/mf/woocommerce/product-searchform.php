<?php
/**
 * The template for displaying product search form 
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
	<label class="screen-reader-text" for="woocommerce-product-search-field"><?php esc_html_e( 'Search for:', 'mf' ); ?></label>
	<input type="search" id="woocommerce-product-search-field" class="search-field" placeholder="<?php echo esc_html_x( 'Search Products&hellip;', 'placeholder', 'mf' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'mf' ); ?>" />
	<input type="submit" value="<?php echo esc_html_x( 'Search', 'submit button', 'mf' ); ?>" />
	<input type="hidden" name="post_type" value="product" />
</form>
