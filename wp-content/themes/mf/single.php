<?php
/**
* The template for displaying all single posts.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package Mf Theme
*/
get_header(); ?>
<div class="content-main">
  <div id="primary" class="site-content">
    <div id="content" role="main">  
     <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'template-parts/post-title'); ?>         
     
      <?php get_template_part( 'template-parts/content', 'single' ); ?>
      <?php endwhile; // End of the loop. ?>
    </div>
    <!-- #content -->
  </div>
  <!-- #primary -->
</div>
<?php get_footer(); ?>
