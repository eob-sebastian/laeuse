<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MF Theme
 */
get_header();
global $mf_options;
$layout = $mf_options['product_layout'];
?>
<div class="content-main">
  <div id="primary" class="site-content">
    <div id="content" role="main" class="ad-woo-bg">  
      <?php
get_template_part('template-parts/woo-title');
?>         
      <section class="contact-block shop container">
        <div class="row shop-description">
          <?php
if ($layout == '0') {
    echo '<div class="col-xs-12">';
} elseif ($layout == '2') {
    echo '<div class="col-xs-12 col-sm-8 col-md-9 col-sm-push-4 col-md-push-3">';
} elseif ($layout == '1') {
    echo '<div class="col-xs-12 col-sm-8 col-md-9">';
}
?>
          <?php
if (have_posts()):
?>
          <?php
    woocommerce_content();
?>
          <?php
endif;
?>
          <?php
echo '</div>';
if ($layout == '2') {
    echo '<aside class="col-xs-12 col-sm-4 col-md-3 col-sm-pull-8 col-md-pull-9">';
    dynamic_sidebar('product_detail_sidebar');
    echo '</aside>';
} elseif ($layout == '1') {
    echo '<aside class="col-xs-12 col-sm-4 col-md-3">';
    dynamic_sidebar('product_detail_sidebar');
    echo '</aside>';
}
?>
        </div>
      </section>
    </div>
    <!-- #content -->
  </div>
  <!-- #primary -->
</div>
<?php
get_footer();
?>
