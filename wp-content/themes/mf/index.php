<?php
/**
* The main template file.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package MF Theme
*/
get_header(); 

if (isset($mf_options['page-title-image']['url'])) {     $page_images = $mf_options['page-title-image']['url'];    }else{     $page_images = '' ;}
if (isset($mf_options['tr-default-pg-bg']['url'])) {     $glpageimage = $mf_options['tr-default-pg-bg']['url'];    }else{     $glpageimage = esc_url_raw( get_template_directory_uri().'/assets/img/default-image.jpg'); ;}

?>
<!-- primary -->
 <div class="page-banner">
          <div class="stretch">
          <?php  if ($page_images == '') { ?>
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($glpageimage); ?>" >
           <?php }else {   ?> 
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($page_images); ?>" >
           <?php } ?> 
          </div>
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="holder">
                  <h1 class="heading text-capitalize"><?php bloginfo( 'name' ); ?></h1>
                 
                </div>
                <ul class="breadcrumbs list-inline">
                 <?php mf_breadcrumbs(); ?>
                </ul>
              </div>
            </div>
          </div>
        </div> 
<div id="primary" class="content-area">
  <!-- main -->
  <main id="main" class="site-main">

    <?php if ( have_posts() ) : ?>
    <?php if ( is_home() && ! is_front_page() ) : ?>

    <?php endif; ?>
    <?php /* Start the Loop */ ?>
    <?php
/*
* Include the Post-Format-specific template for the content.
* If you want to override this in a child theme, then include a file
* called content-___.php (where ___ is the Post Format name) and that will be used instead.
*/
get_template_part( 'template-parts/content' );
?>
    <?php else : ?>
    <?php get_template_part( 'template-parts/content', 'none' ); ?>
    <?php endif; ?>
  </main>
  <!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>