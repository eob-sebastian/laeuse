<?php
global $mf_options;
if (isset($mf_options['mobile-logo']['url'])) {     $mlogo = $mf_options['mobile-logo']['url'];    }else{     $mlogo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'); ;}
$telephone = $mf_options['text-mobile-phone'];
$email = $mf_options['text-mobile-mail'];
?>
<header class="header mob-header cart-true nz-clearfix">
  <div class="mob-header-top nz-clearfix">
    <div class="container">
      <div class="logo logo-mob">
	  <a href="<?php echo esc_url(site_url()); ?>">
        <img src="<?php echo esc_url($mlogo); ?>" alt="">
		 </a>
      </div>
      <span class="mob-menu-toggle">
      </span>
    </div>
  </div>
</header>
<div class="mob-header-content nz-clearfix">
  <span class="mob-menu-toggle2">
  </span>
  <div class="custom-scroll-bar">
    <?php mf_mobile_menu() ?>
    <div class="slogan nz-clearfix">
      <div>
        <div class="nz-bar">
          <span class="nz-icon none small icon-phone animate-false">
          </span>
          <span class="header-top-label">
		  	 <?php   if (isset($mf_options['phone-on-off'])) {     $mobilephone = $mf_options['phone-on-off'];    }else{     $mobilephone = 0; ;} ?>
	 	          <?php if ($mobilephone == '1') { ?>
            <?php echo esc_html($telephone); ?>
		         <?php  }else { echo ""; }?>
          </span>
        </div>
        <div class="nz-bar">
          <span class="nz-icon none small icon-envelope animate-false">
          </span>
		  		  	 <?php   if (isset($mf_options['email-on-off'])) {     $mobilemail = $mf_options['email-on-off'];    }else{     $mobilemail = 0; ;} ?>
	 	          <?php if ($mobilemail == '1') { ?>
		  <a href="mailto:<?php echo esc_html($email); ?>" target="_top"><?php echo esc_html($email); ?></a>
		  		         <?php  }else { echo ""; }?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="mob-overlay">&nbsp;
</div>
