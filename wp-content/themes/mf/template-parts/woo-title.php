  
<?php
global $mf_options;

$header1 = get_template_directory_uri().'/assets/img/header1.png';
$header2 = get_template_directory_uri().'/assets/img/header2.png';
$header3 = get_template_directory_uri().'/assets/img/header3.png';
$header4 = get_template_directory_uri().'/assets/img/header4.png';
$header5 = get_template_directory_uri().'/assets/img/header5.png';
$header6 = get_template_directory_uri().'/assets/img/header6.png';
$header7 = get_template_directory_uri().'/assets/img/header7.png';
$header8 = get_template_directory_uri().'/assets/img/header8.png';
$header9 = get_template_directory_uri().'/assets/img/header9.png';
$header10 = get_template_directory_uri().'/assets/img/header10.png';
$header11 = get_template_directory_uri().'/assets/img/header11.png';
$header12 = get_template_directory_uri().'/assets/img/header12.png';
$header13 = get_template_directory_uri().'/assets/img/header13.png';

if (isset($mf_options['page-title-show'])) {     $ptitleshow = $mf_options['page-title-show'];    }else{     $ptitleshow = 1; ;} 
$pagesubtitle = $mf_options['page-subtitle'];
$new_headers = $mf_options['new-header-version'];
if (isset($mf_options['page-title-image']['url'])) {     $page_images = $mf_options['page-title-image']['url'];    }else{     $page_images = '' ;}
if (isset($mf_options['woo-default-pg-bg']['url'])) {     $glpageimage = $mf_options['woo-default-pg-bg']['url'];    }else{     $glpageimage = esc_url_raw( get_template_directory_uri().'/assets/img/woo-title.jpg'); ;}

?>

<?php if ($new_headers == $header4 || $new_headers == $header5 || $new_headers == $header7 || $new_headers == $header8 || $new_headers == $header9 ) { ?>
  <?php if ($ptitleshow == '1') { ?>
  <!-- page classic -->
        <div class="mf-breadcrumb page-banner  grey small ">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="holder">
                  <h1 class="heading"><?php woocommerce_page_title(); ?></h1>
                </div>
                 <ul class="breadcrumbs list-inline">
                <?php mf_breadcrumbs(); ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <?php }else {} ?>
<?php } ?>

<?php if ($new_headers == $header1 || $new_headers == $header2 || $new_headers == $header3 || $new_headers == $header6  ) { ?>
  <!-- page transparent -->
  <?php if ($ptitleshow == '1') { ?>
        <div class="page-banner woo-tit">
          <div class="stretch">
            <?php  if ($page_images == '') { ?>
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($glpageimage); ?>" >
           <?php }else {   ?> 
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($page_images); ?>" >
           <?php } ?>  
          </div>
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="holder">
                  <h1 class="heading text-capitalize"><?php woocommerce_page_title(); ?></h1>
                  <p><?php echo esc_html($pagesubtitle); ?> </p>
                </div>
                <ul class="breadcrumbs list-inline">
                    <?php mf_breadcrumbs(); ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <?php }else {} ?>
<?php } ?>