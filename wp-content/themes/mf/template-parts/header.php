<?php
global $mf_options;

$header1 = get_template_directory_uri().'/assets/img/header1.png';
$header2 = get_template_directory_uri().'/assets/img/header2.png';
$header3 = get_template_directory_uri().'/assets/img/header3.png';
$header4 = get_template_directory_uri().'/assets/img/header4.png';
$header5 = get_template_directory_uri().'/assets/img/header5.png';
$header6 = get_template_directory_uri().'/assets/img/header6.png';
$header7 = get_template_directory_uri().'/assets/img/header7.png';
$header8 = get_template_directory_uri().'/assets/img/header8.png';
$header9 = get_template_directory_uri().'/assets/img/header9.png';
$header10 = get_template_directory_uri().'/assets/img/header10.png';
$header11 = get_template_directory_uri().'/assets/img/header11.png';
$header12 = get_template_directory_uri().'/assets/img/header12.png';
$header13 = get_template_directory_uri().'/assets/img/header13.png';

?>
<?php get_template_part( 'template-parts/mobile-header' ); ?>

<?php if ($mf_options['new-header-version'] == '') { ?>
   <?php get_template_part( 'template-parts/headers/header1' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header1) { ?>
   <?php get_template_part( 'template-parts/headers/header1' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header2) { ?>
   <?php get_template_part( 'template-parts/headers/header2' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header3) { ?>
   <?php get_template_part( 'template-parts/headers/header3' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header4) { ?>
   <?php get_template_part( 'template-parts/headers/header4' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header5) { ?>
   <?php get_template_part( 'template-parts/headers/header5' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header6) { ?>
   <?php get_template_part( 'template-parts/headers/header6' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header7) { ?>
   <?php get_template_part( 'template-parts/headers/header7' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header8) { ?>
   <?php get_template_part( 'template-parts/headers/header8' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header9) { ?>
   <?php get_template_part( 'template-parts/headers/header9' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header10) { ?>
   <?php get_template_part( 'template-parts/headers/header10' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header11) { ?>
   <?php get_template_part( 'template-parts/headers/header11' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header12) { ?>
   <?php get_template_part( 'template-parts/headers/header12' ); ?>
<?php } ?>

<?php if ($mf_options['new-header-version'] == $header13) { ?>
   <?php get_template_part( 'template-parts/headers/header13' ); ?>
<?php } ?>


