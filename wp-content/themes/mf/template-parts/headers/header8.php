<?php
global $mf_options;
if (isset($mf_options['main-logo-light']['url'])) {     $logo = $mf_options['main-logo-light']['url'];    }else{     $logo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'); ;}
if (isset($mf_options['main-logo-dark']['url'])) {     $logod = $mf_options['main-logo-dark']['url'];    }else{     $logod = esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'); ;}
$logo_select = $mf_options['logo-select'];
$telephone = $mf_options['text-telephone'];
$email = $mf_options['text-mail'];
$social_media = $mf_options['new-social-media'];
$top_menu = $mf_options['new-top-menu'];
$call_show = $mf_options['call-button-show'];
$call_button = $mf_options['call-button-text'];
$call_link = $mf_options['call-button-link'];
?>
<div class="w1">
			<!-- header of the page -->
			<header id="header" class="style12 classic-header adclassic version1 subeffect-slide desk">
				<div class="container">
					<div class="row">
						<!-- header top -->
						<div class="col-xs-12 header-top">
							<ul class="list-inline info-list">
							<?php if ($telephone == '') {  }else { ?>
								<li><a class="tel" href="tel:<?php echo esc_attr($telephone); ?>"><i class="fa fa-phone"></i> <?php echo esc_html($telephone); ?></a></li>
								<?php } ?>
								<?php if ($email == '') {  }else { ?>
								<li><a href="mailto:<?php echo esc_attr($email); ?>"><i class="fa fa-envelope"></i> <?php echo esc_html($email); ?></a></li>
								<?php } ?>
							</ul>
							<!-- language nav -->
							<?php if ($top_menu == '1') { ?>
							<nav class="nav language-nav">
								<?php mf_top_menu() ?>
							</nav>
							 <?php }else {} ?>
						</div>
					</div>
					<div class="row">
						<!-- header center -->
						<div class="col-xs-12 header-cent">
							<!-- page logo -->
							<div class="logo">
                                 <a href="<?php echo esc_url(site_url()); ?>">
                   <?php if ($logo_select == '1') { ?>
                   <img src="<?php echo esc_url($logo); ?>" class="img-responsive w-logo" alt="">
                   <?php }else { ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive w-logo" alt="">
                   <?php } ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive b-logo" alt="">
                </a>
                            </div>
                             <?php if ($social_media == '1') { ?>
							<ul class="list-inline head-social">
								<?php mf_social_media() ?>
							</ul>
							 <?php }else {} ?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
						<!-- main navigation of the page -->
						<nav id="nav">
				                <a href="#" class="nav-opener"><i class="fa fa-bars"></i></a>
				               
				               <?php mf_main_menu() ?>
				                <?php if ($call_show == '1') { ?>
				               <a href="<?php echo esc_url($call_link); ?>" class="quote-btn"><?php echo esc_html($call_button); ?></a>
				               <?php }else {} ?>
				               
				                </nav>
						</div>
					</div>
				</div>
			</header>