<?php
global $mf_options;
if (isset($mf_options['main-logo-light']['url'])) {     $logo = $mf_options['main-logo-light']['url'];    }else{     $logo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'); ;}
if (isset($mf_options['main-logo-dark']['url'])) {     $logod = $mf_options['main-logo-dark']['url'];    }else{     $logod = esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'); ;}
$logo_select = $mf_options['logo-select'];
$social_media = $mf_options['new-social-media'];
?>

			<div class="w7">
			<!-- sidemenu photo -->
			<div class="sidemenu-photo">
				<div class="win-height jcf-scrollable">
					<!-- sidemenu holder -->
					<div class="sidemenu-holder">
						<header id="header7">
							<div class="logo">
							<a href="<?php echo esc_url(site_url()); ?>">
							<?php if ($logo_select == '1') { ?>
                   <img src="<?php echo esc_url($logo); ?>" height="24" width="120" alt="">
                   <?php }else { ?>
                   <img src="<?php echo esc_url($logod); ?>" height="24" width="120" alt="">
                   <?php } ?>
							</a></div>
							<nav id="nav7">
								<a class="nav-opener" href="#">
									<span class="txt">Menu</span>
									<i class="fa fa-bars"></i>
								</a>
								<div class="nav-holder adleft-menu">
									<?php mf_left_menu() ?>
								</div>
							</nav>
						</header>
						<!-- footer of the page -->
						<footer id="footer" class="style12">
							<!-- footer bottom -->
							<div class="footer-bottom">
							 <?php if ($social_media == '1') { ?>
								<ul class="f-social-networks list-inline">
									<?php mf_social_media() ?>
								</ul>
								 <?php }else {} ?>
							
							</div>
						</footer>
					</div>
				</div>
			</div>