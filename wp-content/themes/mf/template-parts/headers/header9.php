<?php
global $mf_options;
if (isset($mf_options['main-logo-light']['url'])) {     $logo = $mf_options['main-logo-light']['url'];    }else{     $logo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'); ;}
if (isset($mf_options['main-logo-dark']['url'])) {     $logod = $mf_options['main-logo-dark']['url'];    }else{     $logod = esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'); ;}
$logo_select = $mf_options['logo-select'];
$telephone = $mf_options['text-telephone'];
$email = $mf_options['text-mail'];
$social_media = $mf_options['new-social-media'];
$searchbox = $mf_options['search-box'];
$cartbox = $mf_options['ad_cart-box'];
?>
<div class="w1">
			<!-- header of the page -->
			<header id="header" class="style18 classic-header hsh adclassic version1 subeffect-slide desk">
				<div class="header-top">
					<div class="container">
						<div class="row">
						<!-- header top -->
							<div class="col-xs-12">
								<ul class="list-inline info-list">
									<?php if ($email == '') {  }else { ?>
									<li><a href="mailto:<?php echo esc_attr($email); ?>"><i class="fa fa-envelope"></i><?php echo esc_html($email); ?></a></li>
									<?php } ?>
									<?php if ($telephone == '') {  }else { ?>
									<li><a href="tel:<?php echo esc_attr($telephone); ?>" class="tel"><i class="fa fa-phone"></i><?php echo esc_html($telephone); ?></a></li>
									<?php } ?>
								</ul>
								 <?php if ($social_media == '1') { ?>
								<ul class="head-socialnetworks list-inline">
									<?php mf_social_media() ?>
								</ul>
								 <?php }else {} ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
						<div class="logo">
							 <a href="<?php echo esc_url(site_url()); ?>">
                   <?php if ($logo_select == '1') { ?>
                   <img src="<?php echo esc_url($logo); ?>" class="img-responsive w-logo" alt="">
                   <?php }else { ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive w-logo" alt="">
                   <?php } ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive b-logo" alt="">
                </a>
						</div>
						<!-- main nav -->
						<div class="holder">
								<!-- icon list -->
								
								<ul class="list-unstyled icon-list">
									 <?php if ($searchbox == '1') { ?>
									<li><a href="#" class="search-opener opener-icons"><i class="fa fa-search"></i></a></li>
									<?php }else {} ?>
									<?php if ($cartbox == '1') { ?>
					                <li class="cart-box">
					                 <a href="#" class="cart-opener opener-icons">
					                 <i class="fa fa-shopping-cart"></i>
					                  <?php get_template_part( 'woocommerce/cart/mini-cart' ); ?>
					                   </a>
					                </li>
					                 <?php }else {} ?>
								</ul>
								  
								<!-- main navigation of the page -->
								
						<nav id="nav">
				                <a href="#" class="nav-opener"><i class="fa fa-bars"></i></a>
				                <div class="nav-holder">
				               <?php mf_main_menu() ?>
				               </div>
				                </nav>
				                </div>
						
						</div>
					</div>
				</div>
			</header>
			<!-- search popup -->
			<div class="search-popup win-height">
				<div class="holder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								 <a href="#" class="close-btn"><?php esc_html__('Close','mf')?></a>
                 				<?php get_search_form(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>