<?php
/**
* The header for our theme.
*
* This is the template that displays all of the <head>
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package MF Theme
*/
?>

<!DOCTYPE html>
<html 
      <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <?php global $mf_options;
if (isset($mf_options['favicon']['url'])) {     $favicon = $mf_options['favicon']['url'];    }else{     $favicon = 'https://s.w.org/favicon.ico' ;}
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
echo ' <link rel="icon" href="'. $favicon .'" />';
}
?>
  <!-- Custom Css -->
  <?php wp_head(); ?> 
    <!-- Custom Css -->
  <style type="text/css">
 <?php  echo isset( $mf_options['opt-ace-editor-css'] ) && $mf_options['opt-ace-editor-css'] !== "" ? $mf_options['opt-ace-editor-css'] : '';?>
  </style>
   <?php
  echo isset( $mf_options['mf-before-head'] ) && $mf_options['mf-before-head'] !== "" ? $mf_options['mf-before-head'] : '';
  ?>
</head>

<?php $layout_boxed = $mf_options['layout-w-b']; ?>
<?php if ($layout_boxed == '0') { 
      $layoutad = 'boxed-v';  
    }else { $layoutad = 'aa'; }; ?>
<body <?php body_class($class = " $layoutad " ); ?>>


       <!-- general wrap start -->
    <div id="wrapper" class="header_fixed header_over">
<?php get_template_part( 'template-parts/headers/header10' ); ?>
<main id="main">