<?php

global $mf_options;
if (isset($mf_options['main-logo-light']['url'])) {     $logo = $mf_options['main-logo-light']['url'];    }else{     $logo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'); ;}
if (isset($mf_options['main-logo-dark']['url'])) {     $logod = $mf_options['main-logo-dark']['url'];    }else{     $logod = esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'); ;}
$logo_select = $mf_options['logo-select'];

?>
    <div class="w1">
<a href="#" class=" portfolio-nav-opener"><i class="fa fa-bars"></i></a>
      <!-- sidenav port style2 -->
      <nav class="sidenav-port style2 nav11">
        <div class="win-height jcf-scrollable adleft-menu">
          <strong class="lancer-logo">
          <a href="<?php echo esc_url(site_url()); ?>">
          	<?php if ($logo_select == '1') { ?>
                   <img src="<?php echo esc_url($logo); ?>" height="24" width="120" alt="">
                   <?php }else { ?>
                   <img src="<?php echo esc_url($logod); ?>" height="24" width="120" alt="">
                   <?php } ?>
          
          </a></strong>
          <?php mf_left_menu() ?>
        </div>
      </nav>
