<?php
global $mf_options;
if (isset($mf_options['main-logo-light']['url'])) {     $logo = $mf_options['main-logo-light']['url'];    }else{     $logo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'); ;}
if (isset($mf_options['main-logo-dark']['url'])) {     $logod = $mf_options['main-logo-dark']['url'];    }else{     $logod = esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'); ;}
$logo_select = $mf_options['logo-select'];
?>
    <div class="w9">
<!-- sidemenu photo v9 -->
			<div class="sidemenu-photo v9">
				<div class="win-height jcf-scrollable">
					<!-- sidemenu holder -->
					<div class="sidemenu-holder">
						<!-- header7 -->
						<header id="header7">
							<div class="logo">
								<a href="<?php echo esc_url(site_url()); ?>">
								<?php if ($logo_select == '1') { ?>
                    <img src="<?php echo esc_url($logod); ?>" height="24" width="120" alt="">	
                   <?php }else { ?>
                   <img src="<?php echo esc_url($logod); ?>" height="24" width="120" alt="">
                   <?php } ?>
								</a>
								</div>
							<nav id="nav7">
								<a class="nav-opener" href="#">
									<i class="fa fa-bars"></i>
								</a>
								<div class="nav-holder adleft-menu">
									<?php mf_left_menu() ?>
								</div>
							</nav>
						</header>
					</div>
				</div>
				<div class="logo-v9">
					 <a href="<?php echo esc_url(site_url()); ?>">
                   <?php if ($logo_select == '1') { ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive b-logo" alt="">
                   <?php }else { ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive b-logo" alt="">
                   <?php } ?>
                
                </a>
				</div>
			</div>