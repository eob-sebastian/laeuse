<?php
global $mf_options;
if (isset($mf_options['main-logo-light']['url'])) {     $logo = $mf_options['main-logo-light']['url'];    }else{     $logo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'); ;}
if (isset($mf_options['main-logo-dark']['url'])) {     $logod = $mf_options['main-logo-dark']['url'];    }else{     $logod = esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'); ;}
$logo_select = $mf_options['logo-select'];
?>
    <div class="w1">
 <div class="logo port-logo">
                 <a href="<?php echo esc_url(site_url()); ?>">
                   <?php if ($logo_select == '1') { ?>
                   <img src="<?php echo esc_url($logo); ?>" class="img-responsive w-logo" alt="">
                   <?php }else { ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive w-logo" alt="">
                   <?php } ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive b-logo" alt="">
                </a>
            </div>
			<a href="#" class="portfolio-nav-opener"><i class="fa fa-bars"></i></a>
			<!-- sidenav port -->
			<nav class="sidenav-port nav11">
				<div class="win-height jcf-scrollable adleft-menu">
					<?php mf_left_menu() ?>
				</div>
			</nav>
