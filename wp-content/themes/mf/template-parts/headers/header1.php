<?php
global $mf_options;
if (isset($mf_options['main-logo-light']['url'])) {     $logo = $mf_options['main-logo-light']['url'];    }else{     $logo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'); ;}
if (isset($mf_options['main-logo-dark']['url'])) {     $logod = $mf_options['main-logo-dark']['url'];    }else{     $logod = esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'); ;}
$logo_select = $mf_options['logo-select'];
$searchbox = $mf_options['search-box'];
$cartbox = $mf_options['ad_cart-box'];
$homeside = isset( $mf_options['home-side-bar'] ) && $mf_options['home-side-bar'] !== "" ? $mf_options['home-side-bar'] : '';

?>

    <div class="w1">
<!-- header of the page -->
      <header id="header" class="version1 style1 ad-light subeffect-slide desk">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!-- page logo -->
              <div class="logo">
                <a href="<?php echo esc_url(site_url()); ?>">
                   <?php if ($logo_select == '1') { ?>
                   <img src="<?php echo esc_url($logo); ?>" class="img-responsive w-logo" alt="">
                   <?php }else { ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive w-logo" alt="">
                   <?php } ?>
                   <img src="<?php echo esc_url($logod); ?>" class="img-responsive b-logo" alt="">
                </a>
              </div>
              <div class="holder">
                <!-- icon list -->
                <ul class="list-unstyled icon-list ml-30">
              
                  <?php if ($searchbox == '1') { ?>
                  <li>
                    <a href="#" class="search-opener opener-icons"><i class="fa fa-search"></i></a>
                  </li>
                  <?php }else {} ?>

                <?php if ($cartbox == '1') { ?>
                <li class="cart-box">
                 <a href="#" class="cart-opener opener-icons">
                 <i class="fa fa-shopping-cart"></i>
                  <?php get_template_part( 'woocommerce/cart/mini-cart' ); ?>
                   </a>
                </li>
                 <?php }else {} ?>
 



              
                </ul>
                <!-- main navigation of the page -->
                <nav id="nav" >
                <a href="#" class="nav-opener"><i class="fa fa-bars"></i></a>
                <div class="nav-holder">
                <?php mf_main_menu() ?>
                </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- search popup -->
    
      <div class="search-popup win-height">
        <div class="holder">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <a href="#" class="close-btn"><?php esc_html__('Close','mf')?></a>
               <?php get_search_form(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
   
    