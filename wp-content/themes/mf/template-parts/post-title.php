  
<?php
global $mf_options;

$header1 = get_template_directory_uri().'/assets/img/header1.png';
$header2 = get_template_directory_uri().'/assets/img/header2.png';
$header3 = get_template_directory_uri().'/assets/img/header3.png';
$header4 = get_template_directory_uri().'/assets/img/header4.png';
$header5 = get_template_directory_uri().'/assets/img/header5.png';
$header6 = get_template_directory_uri().'/assets/img/header6.png';
$header7 = get_template_directory_uri().'/assets/img/header7.png';
$header8 = get_template_directory_uri().'/assets/img/header8.png';
$header9 = get_template_directory_uri().'/assets/img/header9.png';
$header10 = get_template_directory_uri().'/assets/img/header10.png';
$header11 = get_template_directory_uri().'/assets/img/header11.png';
$header12 = get_template_directory_uri().'/assets/img/header12.png';
$header13 = get_template_directory_uri().'/assets/img/header13.png';
  
$ptitleshow = $mf_options['post-title-show'];
$pagesubtitle = $mf_options['post-subtitle'];
$new_headers = $mf_options['new-header-version'];
$breadcrumbs_show = $mf_options['post-breadcrumbs-parent'];
$page_classic_bg = $mf_options['post-classic-bg'];
$page_classic_title_color = $mf_options['post-classic-text'];

if (isset($mf_options['post-title-image']['url'])) {     $page_images = $mf_options['post-title-image']['url'];    }else{     $page_images = '' ;}
if (isset($mf_options['tr-default-pg-bg']['url'])) {     $glpageimage = $mf_options['tr-default-pg-bg']['url'];    }else{     $glpageimage = esc_url_raw( get_template_directory_uri().'/assets/img/default-image.jpg'); ;}

?>

<?php if ($new_headers == $header4 || $new_headers == $header5 || $new_headers == $header7 || $new_headers == $header8 || $new_headers == $header9 || $new_headers == $header7  ) { ?>
  <?php if ($ptitleshow == '1') { ?>
  <!-- page classic -->
        <div class="mf-breadcrumb page-banner  grey small " style="color:<?php echo esc_attr($page_classic_title_color); ?> ; background-color:<?php echo esc_attr($page_classic_bg); ?> ;">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="holder">
                  <h1 style="color:<?php echo esc_attr($page_classic_title_color); ?> ;" class="heading"><?php the_title(); ?></h1>
                </div>
                 <?php  if ($breadcrumbs_show == '1') { ?>
                 <ul class="breadcrumbs list-inline">
                <?php mf_breadcrumbs(); ?>
                </ul>
                  <?php }else {} ?>
              </div>
            </div>
          </div>
        </div>
        <?php }else {} ?>
<?php } else { ?>

<div class="page-banner">
          <div class="stretch">
          <?php  if ($page_images == '') { ?>
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($glpageimage); ?>" >
           <?php }else {   ?> 
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($page_images); ?>" >
           <?php } ?>  
          </div>
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="holder">
                  <h1 class="heading text-capitalize"><?php the_title(); ?></h1>
                  <p><?php echo esc_html($pagesubtitle); ?> </p>
                </div>
                 <?php  if ($breadcrumbs_show == '1') { ?>
                <ul class="breadcrumbs list-inline">
                    <?php mf_breadcrumbs(); ?>
                </ul>
                <?php }else {} ?>
              </div>
            </div>
          </div>
        </div>
    
    <?php } ?>

