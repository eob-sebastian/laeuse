<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package MF Theme
 */

get_header(); 
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" style="padding-top: 45px; padding-bottom: 45px;">
    <div class="container no-padding">
      <article  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
          <section class="c-content-boxes variant-two ">
            <div class="col-md-9 no-padding-left">
              <div class="col-md-12 mt10">
                <section class="error-404 not-found">
                  <article class="text-center">
                    <h2 class="c-font-xl c-margin-b-30">
                      <?php _e( 'Nothing Found', 'mf' ) ?>
                      </span>
                     </h2>
                    <h4 class="c-margin-b-30">
                      <?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'mf' ); ?>
                    </h4>
                    <?php
                   // global $mf_options;
                    //$searchbox = $mf_options['search-on'];
			        // if ($searchbox == '1') { ?>
                    <form method="post" class="form-inline">
                      <div class="input-group input-group-lg">
                        <?php get_search_form(); ?>
                        </div>
                    </form>
                    <?php //}else {} ?>
                  </article>
                </section>
                <!-- .error-404 --> 
              </div>
            </div>
            <div class="col-md-3  no-padding-right">
             <?php dynamic_sidebar( 'default_sidebar' ); ?>
            </div>
          </section>
        </div>
        <!-- .entry-content --> 
        
      </article>
      <!-- #post-## --> 
    </div>
  </main>
  <!-- #main --> 
</div>
<!-- #primary -->

<?php get_footer(); ?>
