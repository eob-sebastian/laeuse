<?php
/**
* Template part for displaying page content in page.php.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package MF Theme
*/
?>
<?php 
// Grab the metadata from the database
global $mf_options;
$layout = $mf_options['layout'];

$p_top = $mf_options['page-padding']['padding-top'];
$p_right = $mf_options['page-padding']['padding-right'];
$p_bottom = $mf_options['page-padding']['padding-bottom'];
$p_left = $mf_options['page-padding']['padding-left'];
if ($p_top == "") { $p_top = "30px";}

$p_padding = $p_top." ".$p_right." ".$p_bottom." ".$p_left;

?>


<div class="container" style="padding: <?php echo esc_attr($p_padding); ?>">
        <div class="row">
          <?php if($layout =='0'){ echo '<div class="col-xs-12">';
              }elseif($layout =='2'){
                echo '<div class="col-xs-12 col-sm-8 col-md-9 col-sm-push-4 col-md-push-3">';
              }elseif($layout =='1'){
                echo '<div class="col-xs-12 col-sm-8 col-md-9">';
              }
          ?>
          
            <?php the_content(); ?>
           
             <?php 
if ( comments_open() || get_comments_number()) {
echo '<div class="comment-box">';
comments_template();
echo '</div>';
}
?>
          <?php 
            echo '</div>';
            if($layout =='2'){
              echo '<aside class="col-xs-12 col-sm-4 col-md-3 col-sm-pull-8 col-md-pull-9">';
              get_sidebar();
              echo '</aside>';
            }elseif($layout =='1'){
              echo '<aside class="col-xs-12 col-sm-4 col-md-3">';
                get_sidebar();
              echo '</aside>';
            }
          ?>
        </div>
            </div>