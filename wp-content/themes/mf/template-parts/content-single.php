<?php
/**
* Template part for displaying single posts.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package Mf Theme
*/
?>
<?php global $mf_options;
// Grab the metadata from the database
$featured_images = $mf_options['post-image'];
$layout = $mf_options['layout-post'];
$author_box = $mf_options['post-box-author'];

$post_top = $mf_options['post-padding']['padding-top'];
$post_right = $mf_options['post-padding']['padding-right'];
$post_bottom = $mf_options['post-padding']['padding-bottom'];
$post_left = $mf_options['post-padding']['padding-left'];


$post_padding = $post_top." ".$post_right." ".$post_bottom." ".$post_left;

?>
<div class="container" style="padding: <?php echo esc_attr($post_padding); ?>">
  <div class="row">
    <?php if($layout =='0'){ echo '<div class="col-xs-12">';
}elseif($layout =='2'){
echo '<div class="col-xs-12 col-sm-8 col-md-9 col-sm-push-4 col-md-push-3">';
}elseif($layout =='1'){
echo '<div class="col-xs-12 col-sm-8 col-md-9">';
}
?>
   
   
    <div class="blog-txt">
      
       <?php if ($featured_images == '1') { ?>
    <?php if ( has_post_thumbnail() ) : ?>
    <div class="img-box">
      <p>
        <a href="<?php the_permalink() ?>">
          <?php the_post_thumbnail(); ?>
        </a>
      </p>
    </div>
    <?php endif; ?>
    <?php }else {} ?>
     <?php get_template_part( '/inc/meta' ); ?>

     
      <p>
        <?php the_content(); ?>
      </p>
       
    </div>
    <div class="col-md-12 plr-no padding-top-60">
    
      <?php
$tags = wp_get_post_tags( $post->ID );
if(!empty($tags)) { ?>
      <div class="post-footer">
       <?php get_template_part( 'inc/share-box' ); ?>
        <div class="post-tags">
          <ul class="list-unstyled">
            <li>
              <i class="fa fa-tags" aria-hidden="true">
              </i>
            </li>
            <?php foreach($tags as $tag) : ?>
            <li class="post-tags-item">
              <a href="<?php echo get_tag_link($tag->term_id); ?>">
                <?php echo esc_attr($tag->name); ?>
              </a>
            </li>
            <?php endforeach; ?>
          </ul>

        </div>

      </div>
      <?php } ?>
          <?php if ($author_box == '1') { ?>
       <div id="author-info" class="clearfix">
            <div class="author-image">
              <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta( 'ID' ))); ?>"><?php echo get_avatar( esc_attr(get_the_author_meta('user_email')), '160', '' ); ?></a>
            </div>   
            <div class="author-bio">
               <h4><?php _e('About The Author', 'mf'); ?></h4>
                <?php the_author_meta('description'); ?>
            </div>
        </div>
        <?php }else {} ?>

         <?php 
if ( comments_open() || get_comments_number()) {
echo '<div class="comment-box">';
comments_template();
echo '</div>';
}
?>
    </div>
 <?php wp_link_pages('before=<p>&after=</p>&next_or_number=number&pagelink=page %'); ?>
    <?php 
echo '</div>';
if($layout =='2'){
echo '<aside class="col-xs-12 col-sm-4 col-md-3 col-sm-pull-8 col-md-pull-9 ">';
get_sidebar();
echo '</aside>';
}elseif($layout =='1'){
echo '<aside class="col-xs-12 col-sm-4 col-md-3 widget ">';
get_sidebar();
echo '</aside>';
}
?>
  </div>
</div>
