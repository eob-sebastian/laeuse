<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MF Theme
 */
global $mf_options;
?>



<div class="container padding-top-100">
  <div class="row">
    <div class="col-xs-12 col-sm-8 col-md-9">
     <?php while ( have_posts() ) : the_post(); ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class( 'blog-post-v1' ); ?>> 
                <div class="img-box">
                 <?php if ( has_post_thumbnail() ) : ?>
                  <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('mf-theme-blog-full'); ?> </a>
                  <?php endif; ?>
                </div>
                <div class="blog-txt">
                  <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                  <?php get_template_part( '/inc/meta' ); ?>
                  <p><?php echo get_the_excerpt(); ?></p>
                  <a href="<?php the_permalink() ?>" class="more"><?php echo esc_html__('Read More...', 'mf'); ?></a>
                  <div class="box-holder">
                   
                    <time datetime="2015-02-18">
                      <span class="add"><?php the_time('j') ?> </span> <?php the_time('F') ?>
                    </time>
                  </div>
                </div>
              </article>
               <?php endwhile; ?>
            <div class="col-xs-12">
              <div class="center">
        <?php
// Previous/next page navigation.
            the_posts_pagination( array(
                'screen_reader_text' => ' ', 
                'prev_text'          => esc_html__( 'Previous page', 'mf' ),
                'next_text'          => esc_html__( 'Next page', 'mf' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'mf' ) . ' </span>',
               
            ) );
  ?>
      </div>
            </div>
    </div>
      <aside class="col-xs-12 col-sm-4 col-md-3">
         <?php dynamic_sidebar( 'default_sidebar' ); ?>
      </aside>
  </div>
</div>