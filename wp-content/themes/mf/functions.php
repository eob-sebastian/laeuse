<?php
/**
 * MF Theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package MF Theme
 */
if (!function_exists('mf_setup')): /**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */ 
    function mf_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on MF Theme, use a find and replace
         * to change 'mf' to the name of your theme in all the template files.
         */
        load_theme_textdomain('mf', get_template_directory() . '/languages');
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');
        // Tell the TinyMCE editor to use a custom stylesheet
        add_editor_style('assets/css/editor-style.css');
        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('mf_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => ''
        )));
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');
        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');
        /* Call Page and Post Metaboxes */
        
        function mf_removePanelCSS()
        {
            wp_dequeue_style('redux-admin-css');
        }
        // This example assumes your opt_name is set to redux_demo, replace with your opt_name value
        add_action('redux/page/mf_options/enqueue', 'mf_removePanelCSS');
        function addAndOverridePanelCSS()
        {
            wp_register_style('redux-custom-css', get_template_directory_uri() . '/inc/framework/css/redux-custom.css', array(), time(), 'all');
            wp_enqueue_style('redux-custom-css');
            
        }
        // This example assumes your opt_name is set to redux_demo, replace with your opt_name value
        add_action('redux/page/mf_options/enqueue', 'addAndOverridePanelCSS', 2);
        
        require_once get_template_directory() . '/inc/framework/barebones-config.php';
        require get_template_directory() . '/inc/framework/config.php';
        require get_template_directory() . '/inc/framework/loader.php';


        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        add_image_size('mf-theme-category-hrz', 270, 190, true);
        add_image_size('mf-theme-blog-full', 845, 510, true);
        add_image_size('mf_image_shop_carousel', 370, 507, true);
        set_post_thumbnail_size(845, 510, true);
        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'main-menu' => esc_html__('Main Menu', 'mf'),
            'top-menu' => esc_html__('Top Menu', 'mf'),
            'left-menu' => esc_html__('Left Menu', 'mf'),
            'footer-menu' => esc_html__('Footer Menu', 'mf')
        ));
        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ));
        /**
         * Starts the list before the elements are added.
         *
         * Adds classes to the unordered list sub-menus.
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param int    $depth  Depth of menu item. Used for padding.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         */
        
    }
endif; // mf_setup
add_action('after_setup_theme', 'mf_setup');
/**
 * Enqueue scripts and styles.
 *
 * @since MF Theme 1.0.0
 */
function mf_scripts()
{
    // THEME STYLES
    // Load our main stylesheet.
    $css_path = get_template_directory_uri() . '/assets/css/style.css';
    wp_enqueue_style('mf-style', $css_path, array(), '1.0');

    // Load our Animation stylesheet
    wp_enqueue_style('mf-animate', get_template_directory_uri() . '/assets/css/animate.css', array(), '1.0.0');

    // Load our helper elements stylesheet
    wp_enqueue_style('mf-helper-elements', get_template_directory_uri() . '/assets/css/helper-elements.css', array(), '1.0.0');
    global $mf_options;
    $ad_skin = $mf_options['general-web-color'];

    // Load our color stylesheet
    wp_enqueue_style('mf-color', get_template_directory_uri() . '/assets/css/color/'. $ad_skin .'', array(), '1.0.0');

    // Load our ui stylesheet
    wp_enqueue_style('mf-menu', get_template_directory_uri() . '/assets/css/menu.css', array(), '1.0.0');

    // Load our woocommerce stylesheet
    wp_enqueue_style('woocommerce', get_template_directory_uri() . '/assets/css/woocommerce.css', array(), '1.0.0');

    //load font awesome
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/includes/font-awesome/css/font-awesome.min.css', array(), '4.5.0');
    
    //load boostrap
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css', array(), '1.0.0');
    
     function insert_jquery()
    {
        wp_enqueue_script('jquery', false, array(), false, false);
    }
    add_filter('wp_enqueue_scripts', 'insert_jquery', 1);
    
    // THEME SCRIPTS
    //load main jq navigasyon
    wp_enqueue_script('mf-navigation', get_template_directory_uri() . '/assets/js/jquery.main.js', array('jquery'), '20120206', true);

    //load controller
    wp_enqueue_script('mf-controller', get_template_directory_uri() . '/assets/js/controller.js', array('jquery'), '20130118', true);

        //load plugins
    wp_enqueue_script('mf-ui', get_template_directory_uri() . '/assets/js/jquery-ui.js', array('jquery'), '20130118', true);

    //load plugins
    wp_enqueue_script('mf-plugins', get_template_directory_uri() . '/assets/js/plugins.js', array('jquery'), '20130118', true);

    //load application
    wp_enqueue_script('mf-app', get_template_directory_uri() . '/assets/js/mf-app.js', array('jquery'), '20130118', true);

    //load boostrap
    wp_enqueue_script('boostrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '20130117', true);


    

   
    


    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
    
    wp_localize_script('mf-script', 'mf_ajax', array(
        'ajaxurl' => admin_url('admin-ajax.php')
    ));
}
add_action('wp_enqueue_scripts', 'mf_scripts');

// Woocommerce
add_action('woocommerce_before_main_content', 'mf_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'mf_wrapper_end', 10);
function mf_wrapper_start()
{
    echo '<section id="main">';
}
function mf_wrapper_end()
{
    echo '</section>';
}
add_action('after_setup_theme', 'woocommerce_support');
function woocommerce_support()
{
    add_theme_support('woocommerce');
}

add_action('admin_head', 'mf_custom_fonts');

function mf_custom_fonts()
{
    echo '<style>
    .redux-notice {
    display: none;
}
  </style>';
}

// Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */



function mf_admin_scripts_styles()
{
    wp_enqueue_script('mf-admin', get_template_directory_uri() . '/includes/custom-menu/js/admin-scripts.js', array(
        'jquery'
    ), '', true);
    wp_enqueue_style('wp-color-picker');
    wp_enqueue_script('wp-color-picker');
    wp_enqueue_script('jquery-ui-spinner');
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');
    wp_enqueue_media();
    
    return;
}
add_action('admin_enqueue_scripts', 'mf_admin_scripts_styles');


function mf_content_width()
{
    $GLOBALS['content_width'] = apply_filters('mf_content_width', 640);
}
add_action('after_setup_theme', 'mf_content_width', 0);
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mf_widgets_init_first()
{
    register_sidebar(array(
        'name' => esc_html__('Default Sidebar', 'mf'),
        'id' => 'default_sidebar',
        'before_widget' => '<aside id="%1$s" class="side-widget"><div class="widget widget_recent_comments">',
        'after_widget' => '</div></aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ));

     register_sidebar(array(
        'name' => esc_html__('Home Hidden Sidebar', 'mf'),
        'id' => 'home_hidden',
        'before_widget' => '<aside id="%1$s" class="side-widget"><div class="widget widget_recent_comments">',
        'after_widget' => '</div></aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ));
    
    register_sidebar(array(
        'name' => esc_html__('Product Sidebar', 'mf'),
        'id' => 'product_detail_sidebar',
        'before_widget' => '<aside id="%1$s" class="side-widget"><div class="widget widget_recent_comments">',
        'after_widget' => '</div></aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ));
    register_sidebar(array(
        'name' => esc_html__('Blog Sidebar', 'mf'),
        'id' => 'blog_sidebar',
        'before_widget' => '<aside id="%1$s" class="c-layout-sidebars-1"><div class="c-area widget %2$s">',
        'after_widget' => '</div></aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 1', 'mf'),
        'id' => 'footer_1',
        'before_widget' => '<aside id="%1$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 2', 'mf'),
        'id' => 'footer_2',
        'before_widget' => '<aside id="%1$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 3', 'mf'),
        'id' => 'footer_3',
        'before_widget' => '<aside id="%1$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 4', 'mf'),
        'id' => 'footer_4',
        'before_widget' => '<aside id="%1$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer Copyright', 'mf'),
        'id' => 'footer_copy',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
}
add_action('widgets_init', 'mf_widgets_init_first');
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
/**
 * Custom breadcrumbs
 */
require get_template_directory() . '/inc/breadcrumbs.php';
/**
 * Get Theme Functions
 */
require get_template_directory() . '/inc/mf-functions.php';

require get_template_directory() . '/includes/custom-menu/custom-menu.php';


require get_template_directory() . '/inc/plugins/tgm-init.php';
function mf_include_files($path)
{
    $files = glob($path);
    if (!empty($files)) {
        foreach ($files as $file) {
            include $file;
        }
    }
}
function mf_page_id()
{
    if (is_page() || is_home() || (class_exists('WooCommerce') && is_shop())) {
        if (is_page()) {
            $post_id = get_the_ID();
        } elseif (is_home()) {
            if (get_option('page_for_posts')) {
                $post_id = get_option('page_for_posts');
            } else {
                return false;
            }
        } else {
            if (get_option('woocommerce_shop_page_id'))
                $post_id = get_option('woocommerce_shop_page_id');
            else
                return false;
        }
    } else {
        return false;
    }
    return $post_id;
}
function mf_header_fixed()
{
    if (mf_page_id()) {
        $post_id = mf_page_id();
        if (get_post_meta($post_id, 'header_fixed', true)) {
            return true;
        }
    } else {
        return false;
    }
}
function mf_header_over()
{
    if (mf_page_id()) {
        $post_id = mf_page_id();
        if (get_post_meta($post_id, 'header_over', true)) {
            return true;
        }
    } else {
        return false;
    }
}

add_action('vc_before_init', 'mf_vcSetAsTheme');
function mf_vcSetAsTheme()
{
    vc_set_as_theme();
}