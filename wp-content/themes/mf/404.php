<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package MF Theme
 */

get_header(); 
if (isset($mf_options['page-title-image']['url'])) {     $page_images = $mf_options['page-title-image']['url'];    }else{     $page_images = '' ;}
if (isset($mf_options['tr-default-pg-bg']['url'])) {     $glpageimage = $mf_options['tr-default-pg-bg']['url'];    }else{     $glpageimage = esc_url_raw( get_template_directory_uri().'/assets/img/default-image.jpg'); ;}
?>
<!-- primary -->
 <div class="page-banner">
          <div class="stretch">
          <?php  if ($page_images == '') { ?>
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($glpageimage); ?>" >
           <?php }else {   ?> 
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($page_images); ?>" >
           <?php } ?> 
          </div>
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="holder">
                  <h1 class="heading text-capitalize"><?php bloginfo( 'name' ); ?></h1>
                 
                </div>
                <ul class="breadcrumbs list-inline">
                 <?php mf_breadcrumbs(); ?>
                </ul>
              </div>
            </div>
          </div>
        </div> 
<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <div class="container no-padding">
      <article  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
          <section class="c-content-boxes variant-two ">
            <div class="col-md-12 no-padding-left">
              <div class="col-md-12 mt10">
                <section class="error-404 not-found">


                <?php if ($mf_options['not-found-text'] == '') { ?>


                <article class="text-center c-margin-t-60 c-margin-b-60" style="margin-top: 60px; margin-bottom: 60px;">
                    <h3 class="c-font-xl c-margin-b-30"><?php echo esc_html__('OOPS! THAT PAGE CAN’T BE FOUND.', 'mf'); ?></h3>
                    <P class="c-margin-b-30">
                      <?php echo esc_html__('It looks like nothing was found at this location. Maybe try a search?', 'mf'); ?>
                    </P>
                
                    <form method="post" class="form-inline">
                      <div class="input-group input-group-lg">
                        <?php get_search_form(); ?>
                     
                    </form>
               
                  </article>



                <?php }else { ?>


                 <article class="text-center c-margin-t-60 c-margin-b-60" style="margin-top: 200px; margin-bottom: 60px;">
                    <h1 class="c-font-xl c-margin-b-30"><?php echo esc_html( $mf_options['not-found-text']);?></h1>
                    <h3 class="c-margin-b-30">
                      <?php echo esc_html( $mf_options['not-found-content']);?>
                    </h3>
                    <?php
                    $searchbox = $mf_options['search-on'];
                    if ($searchbox == '1') { ?>
                    <form method="post" class="form-inline">
                      <div class="input-group input-group-lg">
                        <?php get_search_form(); ?>
                        
                    </form>
                    <?php }else {} ?>
                  </article>

                <?php } ?>


                 
                </section>
                <!-- .error-404 -->
              </div>
            </div>
            <div class="col-md-3  no-padding-right" style="margin-top: 60px;">
              <?php get_sidebar(); ?>
            </div>
          </section>
        </div>
        <!-- .entry-content -->

      </article>
      <!-- #post-## -->
    </div>
  </main>
  <!-- #main -->
</div>
<!-- #primary -->

<?php get_footer(); ?>