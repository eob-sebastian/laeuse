  <?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package MF Theme
*/
global $mf_options;
if (isset($mf_options['footer-logo']['url'])) {     $footer_logo = $mf_options['footer-logo']['url'];    }else{     $footer_logo = esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'); ;}
$social_media = $mf_options['new-social-media'];
?>
<?php $footer_columns = $mf_options['footer-columns']; ?>
<!-- footer of the page -->
<footer id="footer" class="style3">
<?php if ($mf_options['footer-top'] == '1') { ?>
  <!-- footer top -->
  <div class="footer-top bg-dark-jungle">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="holder">
            <div class="logo">
              <a href="#">
                <img src="<?php echo esc_url($footer_logo); ?>" alt="">
              </a>
            </div>
            <!-- footer-social -->
            <ul class="list-inline footer-social">
              <?php mf_social_media() ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php  }else { echo ""; }?>
  <?php if ($mf_options['footer-middle'] == '1') { ?>
  <!-- footer cent -->
  <div class="footer-cent bg-shark">
    <div class="container">
      <div class="row">
        <?php if ($footer_columns == '1') { ?>
        <div class="col-md-12 col-sm-6">
          <?php dynamic_sidebar( 'footer_1' ); ?>
        </div>
        <?php }elseif ($footer_columns == '2') {?>
        <div class="col-md-6 col-sm-6">
          <?php dynamic_sidebar( 'footer_1' ); ?>
        </div>
        <div class="col-md-6 col-sm-6">
          <?php dynamic_sidebar( 'footer_2' ); ?>
        </div>
        <?php }elseif ($footer_columns == '3') {?>
        <div class="col-md-4 col-sm-6">
          <?php dynamic_sidebar( 'footer_1' ); ?>
        </div>
        <div class="col-md-4 col-sm-6">
          <?php dynamic_sidebar( 'footer_2' ); ?>
        </div>
        <div class="col-md-4 col-sm-6">
          <?php dynamic_sidebar( 'footer_3' ); ?>
        </div>
        <?php }else { ?>
        <div class="col-md-3 col-sm-6">
          <?php dynamic_sidebar( 'footer_1' ); ?>
        </div>
        <div class="col-md-3 col-sm-6">
          <?php dynamic_sidebar( 'footer_2' ); ?>
        </div>
        <div class="col-md-3 col-sm-6">
          <?php dynamic_sidebar( 'footer_3' ); ?>
        </div>
        <div class="col-md-3 col-sm-6">
          <?php dynamic_sidebar( 'footer_4' ); ?>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
    <?php  }else { echo ""; }?>
	 <?php   if (isset($mf_options['footer-bottom'])) {     $footerbottom = $mf_options['footer-bottom'];    }else{     $footerbottom = 1; ;} ?>
	 	  <?php if ($footerbottom == '1') { ?>
  <!-- footer bottom -->
  <div class="bg-dark-jungle-bottom footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="bottom-box1">
            <!-- footer-nav -->
            <?php mf_footer_menu() ?>
            <span class="copyright">
              <?php dynamic_sidebar( 'footer_copy' ); ?>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
      <?php  }else { echo ""; }?>
</footer>
<!-- END C-LAYOUT-FOOTER -->
</div>
<div class="fa fa-chevron-up" id="gotoTop" style="display: none;">
</div>
</div>
<?php
echo isset( $mf_options['mf-before-body'] ) && $mf_options['mf-before-body'] !== "" ? $mf_options['mf-before-body'] : '';
?>
<?php wp_footer(); ?>
</body>
</html>
