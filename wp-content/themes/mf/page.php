<?php
/**
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site may use a
* different template.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package MF Theme
*/
get_template_part( 'template-parts/page-headers');
$pagebgcolor = $mf_options['page-bg-color'];

$page_gl_titlebg = $mf_options['tr-default-pg-bg']['url'];
?>   
<div class="content-main">
  <div id="primary" class="site-content">
    <div id="content" role="main" style="background-color: <?php echo esc_attr($pagebgcolor); ?>">  
      <?php get_template_part( 'template-parts/page-title'); ?>         
      <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'template-parts/content', 'page' ); ?>
      <?php endwhile; // End of the loop. ?>
      <?php wp_link_pages('before=<p>&after=</p>&next_or_number=number&pagelink=page %'); ?>
    </div>
    <!-- #content -->
  </div>
  <!-- #primary -->
</div>
<?php get_footer(); ?>
