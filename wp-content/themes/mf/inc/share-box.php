
<div class="sharebox clearfix">
	<ul>
		
		<li>
			<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" class="share-facebook" target="_blank" title="<?php esc_html__( 'Share via', 'mf' ) ?> <?php esc_html__( 'Facebook', 'mf' ) ?>"><i class="fa fa-facebook"></i> <?php esc_html__( 'Facebook', 'mf' ) ?></a>
		</li>
	
	
		<li>
			<a href="http://twitter.com/home?status=<?php the_title(); ?> <?php the_permalink(); ?>" class="share-twitter" target="_blank" title="<?php esc_html__( 'Share via', 'mf' ) ?> <?php esc_html__( 'Twitter', 'mf' ) ?>"><i class="fa fa-twitter"></i> <?php esc_html__( 'Twitter', 'mf' ) ?></a>
		</li>
	
	
		<li>
			<a href="http://www.reddit.com/submit?url=<?php the_permalink() ?>&amp;title=<?php echo urlencode(esc_attr(the_title('', '', false))); ?>" class="share-tumblr" target="_blank" title="<?php esc_html__( 'Share via', 'mf' ) ?> <?php esc_html__( 'Tumblr', 'mf' ) ?>"><i class="fa fa-tumblr"></i> <?php esc_html__( 'Tumblr', 'mf' ) ?></a>
		</li>
	
		
		<li>
			<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo esc_url($url); ?>&amp;" target="_blank" class="share-pinterest" title="<?php esc_html__( 'Share via', 'mf' ) ?> <?php esc_html__( 'Pinterest', 'mf' ) ?>"><i class="fa fa-pinterest"></i> <?php esc_html__( 'Pinterest', 'mf' ) ?></a>
		</li>
		
		
		<li>
			<a href="https://plus.google.com/share?url=<?php the_permalink() ?>" target="_blank" class="share-google" title="<?php esc_html__( 'Share via', 'mf' ) ?> <?php esc_html__( 'Google+', 'mf' ) ?>"><i class="fa fa-google-plus"></i> <?php esc_html__( 'Google+', 'mf' ) ?></a>
		</li>
	
		<li>
			<a href="http://linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink();?>&amp;title=<?php the_title();?>" target="_blank" class="share-linkedin" title="<?php esc_html__( 'Share via', 'mf' ) ?> <?php esc_html__( 'LinkedIn', 'mf' ) ?>"><i class="fa fa-linkedin"></i> <?php esc_html__( 'LinkedIn', 'mf' ) ?></a>
		</li>
		
		<li>
			<a href="mailto:?subject=<?php the_title();?>&amp;body=<?php the_permalink() ?>" class="share-mail" title="<?php esc_html__( 'Share via', 'mf' ) ?> <?php esc_html__( 'E-Mail', 'mf' ) ?>"><i class="fa fa-envelope-o"></i> <?php esc_html__( 'E-Mail', 'mf' ) ?></a>
		</li>
		
	</ul>
</div>