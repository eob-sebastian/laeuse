<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "mf_options";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */


    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'MF Options', 'mf' ),
        'page_title'           => esc_html__( 'MF Options', 'mf' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => get_template_directory_uri().'/inc/framework/css/img/mf-icon.png',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.
         'templates_path'            => get_template_directory() . '/inc/framework/templates/panel',

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );


    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => esc_html__( 'Documentation', 'mf' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => esc_html__( 'Support', 'mf' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => esc_html__( 'Extensions', 'mf' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( esc_html__( '', 'mf' ), $v );
    } else {
        $args['intro_text'] = esc_html__( '', 'mf' );
    }

    // Add content after the form.
    $args['footer_text'] = esc_html__( '', 'mf' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    $vheader1 = get_template_directory_uri().'/assets/img/header1.png';
    $vheader2 = get_template_directory_uri().'/assets/img/header2.png';
    $vheader3 = get_template_directory_uri().'/assets/img/header3.png';
    $vheader4 = get_template_directory_uri().'/assets/img/header4.png';
    $vheader5 = get_template_directory_uri().'/assets/img/header5.png';
    $vheader6 = get_template_directory_uri().'/assets/img/header6.png';
    $vheader7 = get_template_directory_uri().'/assets/img/header7.png';
    $vheader8 = get_template_directory_uri().'/assets/img/header8.png';
    $vheader9 = get_template_directory_uri().'/assets/img/header9.png';
    $vheader10 = get_template_directory_uri().'/assets/img/header10.png';
    $vheader11 = get_template_directory_uri().'/assets/img/header11.png';
    $vheader12 = get_template_directory_uri().'/assets/img/header12.png';
    $vheader13 = get_template_directory_uri().'/assets/img/header13.png';

 
  
    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields
         Redux::setSection( $opt_name, array(
        'icon' => 'el el-laptop',
        'icon_class' => 'icon-large',
        'title' => esc_html__('Start & Help', 'mf'),
        'desc' => esc_html__('', 'mf'),
        'fields' => array(
            array(
                'id' => 'font_awesome_info',
                'type' => 'info',
                'style' => 'warning',
                'desc'     => '<h4>Welcome to the Options panel of the MF</h4><br>
           For any Support, help needs & free demo data install service write us an email to <b>support@mfdsgn.com</b>  , our support team will quickly help you.<br>
		              Online Theme documentation url is: <a href="http://www.mfdsgn.com/documentation/" target="_new">MF Online Documentation</a><br><br>
		   Before importing Demo Data:<br>
		   Check limits on wp-admin >> Tools >> Redux framework >> Status page<br>
		   <li>WP Memory Limit: 512 MB ==> add this code to middle of wp-config.php >> <b>define("WP_MEMORY_LIMIT", "512M");</b></li>
		   <li>PHP Version: 5.4 or higher</li>
		   <li>PHP Memory Limit: 256 MB or higher</li>
		   <li>PHP Post Max Size: 64 MB or higher</li>
		   <li>PHP Time Limit: 300 or higher</li>
		   <li>PHP Max Input Vars: 1000 or higher</li>
		   <li>Max Upload Size: 64 MB or higher</li> <br>
		   After importing Demo Data:<br>
		   <li>Go to wp-admin >> Ess. Grid >> Import/Export >>  Click to Import Demo Grids & Click to Import Full Demo Data</li>
		   <li>Go to wp-admin >> The Grid >> Import/Export >>  Click to Read Grid demo content >> Select All >> Import selected items</li>
		   <li>Go to wp-admin >> Massive Panel >> Install All <b>Style Presets</b>  Install All <b>Content Presets</b> </li>
		   <br><br>
		   If you like <strong>MF</strong> please leave us a <a href="https://themeforest.net/downloads" target="_blank" class="wc-rating-link" data-rated="Thanks :)">&#9733;&#9733;&#9733;&#9733;&#9733;</a> rating. A huge thanks in advance!<br>
		   ',
          ),
        )
    ) );

     Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Layout & Color', 'mf' ),
        'id'         => 'general-setting',
        'icon'   => 'el el-magic',
        'desc'       => esc_html__('', 'mf'),
        'fields'     => array(
           
           
           array(
                'id'       => 'layout-w-b',
                'type'     => 'switch',
                'title'    => esc_html__( 'Layout', 'mf' ),
                'default'        => 1,
                'on'       => 'Wide',
                'off'      => 'Boxed',
            ),
           array(
                'id'       => 'default-color',
                'type'     => 'info',
                'style' => 'warning',
                'desc'    => esc_html__( 'Color', 'mf' ),
            ),
           array(
                'id'       => 'general-web-color',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Predefined Color Scheme', 'mf' ),
                'subtitle' => esc_html__( 'Controls the main color scheme throughout the theme. Select a scheme and all color options will change to the defined selection.', 'mf' ),
                'default'        => 'custom-color.php',
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    'custom-color.php' => get_template_directory_uri().'/assets/img/color/no-color.jpg',
                    'awesome.css' => get_template_directory_uri().'/assets/img/color/awesome.jpg',
                    'color-backery.css' => get_template_directory_uri().'/assets/img/color/color-backery.jpg',
                    'bleu-de-france.css' => get_template_directory_uri().'/assets/img/color/bleu-de-france.jpg',
                    'rich-electric-blue.css' => get_template_directory_uri().'/assets/img/color/rich-electric-blue.jpg',
                    'chateau-green.css' => get_template_directory_uri().'/assets/img/color/chateau-green.jpg',
                    'niagara.css' => get_template_directory_uri().'/assets/img/color/niagara.jpg',
                    'deep-lilac.css' => get_template_directory_uri().'/assets/img/color/deep-lilac.jpg',
                    'dark-pastel-red.css' => get_template_directory_uri().'/assets/img/color/dark-pastel-red.jpg',
                    'orange.css' => get_template_directory_uri().'/assets/img/color/orange.jpg',
                    'my-sin.css' => get_template_directory_uri().'/assets/img/color/my-sin.jpg',

                ),
               
            ),
            array(
                    'id'       => 'general-custom-web-color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Primary Color', 'mf' ),
                    'subtitle' => esc_html__( 'Controls the main highlight color throughout the theme.', 'mf' ),

                    'default'  => '#2f7cbf',
                ),
            )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header', 'mf' ),
        'id'         => 'pt-bar-header',
	    'icon'   => 'el el-home',

       
    ) );
	
	
	
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Header', 'mf' ),
		 'subsection' => true,
        'id'     => 'basic',

        'fields'     => array(		
               array(
                'id'      => 'new-header-version',
                'type'    => 'select_image',
                'presets' => true,
                'title'   => esc_html__( 'Header Version', 'mf' ),
                'default' => $vheader1,
                'options' => array(
                    '0' => array(
                        'alt' => 'Header Default',
                        'img' => $vheader8,
                    ),
                    '1' => array(
                        'alt' => 'Header 1',
                        'img' => $vheader1,
                    ),
                    '2' => array(
                        'alt' => 'Header 2',
                        'img' => $vheader2,
                        
                    ),
                    '3' => array(
                        'alt' => 'Header 3',
                        'img' => $vheader3,
                    ),
                    '4' => array(
                        'alt' => 'Header 4',
                        'img' => $vheader4,
                    ),
                    '5' => array(
                        'alt' => 'Header 5',
                        'img' => $vheader5,
                    ),
                    '6' => array(
                        'alt' => 'Header 6',
                        'img' => $vheader6,
                    ),
                    '7' => array(
                        'alt' => 'Header 7',
                        'img' => $vheader7,
                    ),
                    '8' => array(
                        'alt' => 'Header 8',
                        'img' => $vheader8,
                    ),
                    '9' => array(
                        'alt' => 'Header 9',
                        'img' => $vheader9,
                    ),
                    '10' => array(
                        'alt' => 'Header 10',
                        'img' => $vheader10,
                    ),
                    '11' => array(
                        'alt' => 'Header 11',
                        'img' => $vheader11,
                    ),
                    '12' => array(
                        'alt' => 'Header 12',
                        'img' => $vheader12,
                    ),
                    '13' => array(
                        'alt' => 'Header 13',
                        'img' => $vheader13,
                    ),     
                ),
            ),
            array(
                'id'       => 'logo-select',
                'type'     => 'select',
                'title'    => esc_html__( 'Logo Select', 'mf' ),
                 'subtitle' => esc_html__('You can add the logo from the logo section.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader1,$vheader2,$vheader3,$vheader4,$vheader5,$vheader6,$vheader7,$vheader8,$vheader9,$vheader10,$vheader11,$vheader12,$vheader13 )),
                'options'  => array(
                    '1' => 'Logo Light',
                    '2' => 'Logo Dark'),
                'default'  => 1,
            ),
			             array(
                'id'       => 'header_gb4',
                'type'     => 'background',
                'title'    => esc_html__('Header Background Color/Image', 'mf'),
                'output'   => array( '#header.style4' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader4 )),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#FFFFFF',
                )
            ),
            array(
                'id'       => 'header_gb7',
                'type'     => 'background',
                'title'    => esc_html__('Header Background Color/Image', 'mf'),
                'output'   => array( '#header.style8' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader7 )),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#FFFFFF',
                )
            ),
            array(
                'id'       => 'header_gb8-top',
                'type'     => 'background',
                'title'    => esc_html__('Header Top Bg Color/Image', 'mf'),
                'output'   => array( '#header.style12 .header-top:after' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader8 )),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#f4f4f4',
                )
            ),
            array(
                'id'       => 'header_gb8',
                'type'     => 'background',
                'title'    => esc_html__('Header Background Color/Image', 'mf'),
                'output'   => array( '#header .header-cent:after' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader8 )),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#FFFFFF',
                )
            ),
            array(
                'id'       => 'header_gb8-bottom',
                'type'     => 'background',
                'title'    => esc_html__('Header Menu Bg Color/Image', 'mf'),
                'output'   => array( '#header.style12 #nav:after' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader8 )),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#72aadb',
                )
            ),
            array(
                'id'       => 'header_gb9-top',
                'type'     => 'background',
                'title'    => esc_html__('Header Top Background Color/Image', 'mf'),
                'output'   => array( '#header.style18 .header-top' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader7,$vheader9 )),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#f4f4f4',
                )
            ),
            array(
                'id'       => 'header_gb9',
                'type'     => 'background',
                'title'    => esc_html__('Header Background Color/Image', 'mf'),
                'output'   => array( '#header.style18' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader9 )),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#FFFFFF',
                )
            ),

            array(
                'id'       => 'header_background-h6',
                'type'     => 'background',
                'output'    => array( '#header.style7 .container .row' ),
                'title'    => esc_html__('Header Background Color/Image', 'mf'),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader5 )),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#FFFFFF',
                )
            ),
            array(
                'id'       => 'search-box',
                'type'     => 'switch',
                'title'    => esc_html__( 'Search Box', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader1,$vheader2,$vheader3,$vheader4,$vheader5,$vheader6,$vheader7,$vheader9 )),
                'default'  => true,
            ),
            array(
                'id'       => 'ad_cart-box',
                'type'     => 'switch',
                'title'    => esc_html__( 'Woocommerce Cart Icon in Main Menu', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader1,$vheader2,$vheader3,$vheader4,$vheader5,$vheader6,$vheader7,$vheader9 )),
                'default'  => false,
            ),
           
            array(
                'id'       => 'new-social-media',
                'type'     => 'switch',
                'title'    => esc_html__( 'Social Media', 'mf' ),
                'subtitle' => esc_html__('You can link from the social media section', 'mf'),
                'required' => array('new-header-version','=', array( $vheader2,$vheader3,$vheader13,$vheader6,$vheader8,$vheader9 )),
                'default'  => true,
            ),
            array(
                'id'       => 'new-top-menu',
                'type'     => 'switch',
                'title'    => esc_html__( 'Top Menu', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader2,$vheader7,$vheader8 )),
                'default'  => true,
            ),
            array(
                'id'       => 'text-telephone',
                'type'     => 'text',
                'title'    => esc_html__( 'Phone Number', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader6,$vheader8,$vheader9 )),
                'default'  => '+1.1234567',
            ),
            array(
                'id'       => 'text-mail',
                'type'     => 'text',
                'title'    => esc_html__( 'Email Address', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader6,$vheader8,$vheader9 )),
                'default'  => 'info@domain.com',
            ),
            array(
                'id'       => 'call-button-show',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Call Button', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader8 )),
                'default'  => true,
            ),
			   array( 
				'id'       => 'header_gb8-call-bg',
                'type'     => 'background',
                'title'    => esc_html__('Call Button Color/Image', 'mf'),
                'output'   => array( '#header.style12 #nav .quote-btn' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader8 )),
                 'required' => array('call-button-show','=','1'),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'background-color' => '#222',
                )
            ),
            array(
                'id'       => 'header_gb8-call-text',
                'type'     => 'color',
                'title'    => esc_html__('Call Text Color', 'mf'),
                'output'   => array( '#header.style12 #nav .quote-btn' ),
                'subtitle' => esc_html__('Header background with image, color, etc.', 'mf'),
                'required' => array('new-header-version','=', array( $vheader8 )),
                 'required' => array('call-button-show','=','1'),
                'desc'     => esc_html__('', 'mf'),
                'default'  => array(
                    'color' => '#FFFFFF',
                )
            ),
            array(
                'id'       => 'call-button-text',
                'type'     => 'text',
                'title'    => esc_html__( 'Call Button Text', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader8 )),
                 'required' => array('call-button-show','=','1'),
                'default'  => 'BUY NOW',
            ),
             array(
                'id'       => 'call-button-link',
                'type'     => 'text',
                'title'    => esc_html__( 'Call Button Link', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader8 )),
                 'required' => array('call-button-show','=','1'),
                'default'  => '#',
            ),
              array(
                'id'       => 'logo-margin-h1',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader1 )),
                'output'   => array( '#header.style1 .logo' ),
                'default'  => array(
                    'margin-top'     => '0px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '0px', 
                    'margin-left'    => '0px',
                )
            ),
             array(
                'id'       => 'logo-margin-h2',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader2 )),
                'output'   => array( '#header.style2 .logo' ),
                'default'  => array(
                    'margin-top'     => '-5px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '0px', 
                    'margin-left'    => '0px',
                )
            ),
             array(
                'id'       => 'logo-margin-h3',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader3 )),
                'output'   => array( '#header.style3 .logo' ),
                'default'  => array(
                    'margin-top'     => '0px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '0px', 
                    'margin-left'    => '0px',
                )
            ),
             array(
                'id'       => 'logo-margin-h4',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader4 )),
                'output'   => array( '#header.style4 .logo' ),
                'default'  => array(
                    'margin-top'     => '0px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '0px', 
                    'margin-left'    => '0px',
                )
            ),
             array(
                'id'       => 'logo-margin-h5',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader5 )),
                'output'   => array( '#header.style7 .logo' ),
                'default'  => array(
                    'margin-top'     => '20px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '5px', 
                    'margin-left'    => '16px',
                )
            ),
             array(
                'id'       => 'logo-margin-h6',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader6 )),
                'output'   => array( '#header.style15 .logo' ),
                'default'  => array(
                    'margin-top'     => '0px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '0px', 
                    'margin-left'    => '0px',
                )
            ),
             array(
                'id'       => 'logo-margin-h7',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader7 )),
                'output'   => array( '#header.style8 .logo' ),
                'default'  => array(
                    'margin-top'     => '-5px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '0px', 
                    'margin-left'    => '0px',
                )
            ),
             array(
                'id'       => 'logo-margin-h8',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader8 )),
                'output'   => array( '#header.style12 .logo' ),
                'default'  => array(
                    'margin-top'     => '0px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '0px', 
                    'margin-left'    => '0px',
                )
            ),
             array(
                'id'       => 'logo-margin-h9',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader9 )),
                'output'   => array( '#header.style18 .logo' ),
                'default'  => array(
                    'margin-top'     => '0px', 
                    'margin-right'   => '0px', 
                    'margin-bottom'  => '0px', 
                    'margin-left'    => '0px',
                )
            ),
             array(
                'id'       => 'logo-margin-h10',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader10 )),
                'output'   => array( '.lancer-logo' ),
                'default'  => array(
                    'margin-top'     => '5px', 
                    'margin-right'   => 'auto', 
                    'margin-bottom'  => '50px', 
                    'margin-left'    => 'auto',
                )
            ),
             array(
                'id'       => 'logo-margin-h12',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader12 )),
                'output'   => array( '.sidemenu-photo.v9 .logo' ),
                'default'  => array(
                    'margin-top'     => '0px', 
                    'margin-right'   => 'auto', 
                    'margin-bottom'  => '40px', 
                    'margin-left'    => 'auto',
                )
            ),
             array(
                'id'       => 'logo-margin-h13',
                'type'     => 'spacing',
                'mode'           => 'margin',
                'units'          => array('px'),
                'title'    => esc_html__( 'Logo Margin', 'mf' ),
                'required' => array('new-header-version','=', array( $vheader12 )),
                'output'   => array( '.sidemenu-photo .logo' ),
                'default'  => array(
                    'margin-top'     => '0px', 
                    'margin-right'   => 'auto', 
                    'margin-bottom'  => '97px', 
                    'margin-left'    => 'auto',
                )
            ),
		),
            
      ));

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Mobile Header', 'mf' ),
        'id'         => 'opt-text-mobile-header-options',
		'subsection' => true,
        'fields'     => array(
		           array(
                    'id' => 'phone-on-off',
                    'type' => 'switch',
                    'title' => esc_html__('Show Phone', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the Phone on Mobile Menu.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),
		           array(
                    'id' => 'email-on-off',
                    'type' => 'switch',
                    'title' => esc_html__('Show Email', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the Email on Mobile Menu.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),
				array(
                'id'       => 'text-mobile-phone',
                'type'     => 'text',
                'title'    => esc_html__( 'Phone Number', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '+1.1234567',
            ),
            array(
                'id'       => 'text-mobile-mail',
                'type'     => 'text',
                'title'    => esc_html__( 'Email Address', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => 'info@domain.com',
            ),  
              array(
                'id'       => 'mobile-header-bg-color',
                'type'     => 'color',
                'output'    => array( '.mob-header-top' ),
                'title'    => esc_html__( 'Mobile Header Backgorund Color', 'mf' ),
                'default'  => '#FFFFFF',
                'mode'     => 'background',
            ),
               array(
                'id'       => 'mobile-menu-bg-color',
                'type'     => 'color',
                'output'    => array( '.mob-header-content' ),
                'title'    => esc_html__( 'Mobile Menu Backgorund Color', 'mf' ),
                'default'  => '#FFFFFF',
                'mode'     => 'background',
            ),	
               array(
                'id'       => 'mobile-menu-link-color',
                'type'     => 'color',
                'output'    => array( '.mob-menu li a, .mob-header-content .header-top-menu ul li a' ),
                'title'    => esc_html__( 'Mobile Menu Link Color', 'mf' ),
                'default'  => '#999999',
                'mode'     => 'color',
            ),	
               array(
                'id'       => 'mobile-menu-content-color',
                'type'     => 'color',
                'output'    => array( '.mob-header-content .slogan' ),
                'title'    => esc_html__( 'Mobile Menu Content Color', 'mf' ),
                'default'  => '#999999',
                'mode'     => 'color',
            ),
               array(
                'id'       => 'mobile-menu-line-color',
                'type'     => 'border',
                'output'    => array( '.mob-menu li a, .mob-header-content .header-top-menu ul li a ' ),
                'title'    => esc_html__( 'Mobile Menu Line Color', 'mf' ),
                'default'  => array(
                    'border-color'  => '#999999',
                    'border-style'  => 'solid',
                    'border-top'    => '0',
                    'border-right'  => '0',
                    'border-bottom' => '1px',
                    'border-left'   => '0'
                )
                
            ),	
			
            
        ),

    ) );



     Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Logo & Favicon', 'mf' ),
        'id'         => 'logo-favicon',
        'icon'   => 'el el-bookmark',
        'desc'       => esc_html__('', 'mf'),
        'fields'     => array(
        	array(
                'id'             => 'logo-width',
                'type'           => 'dimensions',
                'output'    => array( '.logo img' ),
                'units'          => array( 'em', 'px', '%' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => esc_html__( 'Logo (Width)', 'mf' ),
                'height'         => false,
                'default'        => array(
                    'width'  => 200,
                )
            ),
           
            array(
                'id'       => 'logo-padding',
                'type'     => 'info',
                'title'    => esc_html__( 'Logo Margin', 'mf' ),

                // An array of CSS selectors to apply this font style to
                'desc'     => esc_html__( 'You can change this setting at Header.', 'mf' ),

            ),
            array(
                'id'       => 'main-logo-light',
                'type'     => 'media',
                'title'    => esc_html__( 'Logo Light', 'mf' ),
                'output' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'mf'),
                'subtitle' => esc_html__('', 'mf'),
                 'default'  => array(
                            'url'=> esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'),
                        ),
             
            ),
            array(
                'id'       => 'main-logo-dark',
                'type'     => 'media',
                'title'    => esc_html__( 'Logo Dark', 'mf' ),
                'output' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'mf'),
                'subtitle' => esc_html__('', 'mf'),
                 'default'  => array(
                            'url'=> esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'),
                        ),
             
            ),
            array(
                'id'             => 'mobile-logo-width',
                'type'           => 'dimensions',
                'output'    => array( '.logo-mob img' ),
                'units'          => array( 'em', 'px', '%' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => esc_html__( 'Mobile Logo (Width)', 'mf' ),
                'height'         => false,
                'default'        => array(
                    'width'  => 100,
                )
            ),
            array(
                'id'       => 'mobile-logo',
                'type'     => 'media',
                'title'    => esc_html__( 'Mobile Logo', 'mf' ),
                'output' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'mf'),
                'subtitle' => esc_html__('', 'mf'),
                'default'  => array(
                            'url'=> esc_url_raw( get_template_directory_uri().'/assets/img/mf-dark.png'),
                        ),
            ),

            array(
                'id'       => 'favicon',
                'type'     => 'media',
                'title'    => esc_html__( 'Favicon', 'mf' ),
                'output' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'mf'),
                'subtitle' => esc_html__('', 'mf'),
                ),

             array(
                'id'       => 'apple-icon',
                'type'     => 'media',
                'title'    => esc_html__( 'Apple iPhone Icon', 'mf' ),

                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => esc_html__('', 'mf'),
                'subtitle' => esc_html__('', 'mf'),
                ),
            )
    ) );



         // Menu
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Menu', 'mf' ),
        'id'         => 'opt-menu-styling',
        'icon'   => 'el el-th-list',
        'fields'     => array(

             array(
                'id'       => 'opt-menu-link-color',
                'type'     => 'typography',
                'output' => array('.version1 .desk-menu > ul > li > a'),
                'title'    => esc_html__( 'Menu Font', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'google'   => true,
                'color'    => false,
                'line-height' => false,                
                'default'  => array(
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '13px',
                ),
            ),
              array(
                'id'       => 'info_trans_menu',
                'type'     => 'info',#nav li.hover > a, #nav li:hover > a, #nav li.active > a
                'style' => 'warning',
                'desc'    => esc_html__( 'Transparent Menu', 'mf' ),
            ),
	        array(
                'id'       => 'add_clasic_spac',
                'type'     => 'spacing',
                'output'   => array( '#nav .nav-top > li' ),
                'mode'     => 'margin',
                'all'      => true,
                'all'      => false,
                'right'         => false,     // Disable the right
                'top'          => false,     // Disable the left
                'bottom'          => false,     // Disable the left
                'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
                'title'    => __( 'Menu Spacing', 'mf' ),
                'default'  => array(
                    'margin-left'    => '30px',

                )
            ),
            array(
                    'id'       => 'ad-menu-transparent-color',
                    'type'     => 'color',
                    'output'    => array('#nav a'),
                    'title'    => esc_html__( 'Menu Color', 'mf' ),
                    'mode'     => 'color',
                    'default'  => '#FFFFFF',
                ),
            array(
                    'id'       => 'ad-menu-transparent-color-hover',
                    'type'     => 'color',
                    'output'    => array('#nav li.hover > a, #nav li:hover > a, #nav li.active > a'),
                    'title'    => esc_html__( 'Menu Hover Color', 'mf' ),
                    'mode'     => 'color',
                ),
             array(
                'id'       => 'info_classic_menu',
                'type'     => 'info',
                'style' => 'warning',
                'desc'    => esc_html__( 'Classic Menu', 'mf' ),
            ),


             array(
                    'id'       => 'ad-menu-Classic-color',
                    'type'     => 'color',
                    'output'    => array('#header.adclassic #nav .nav-top > li > a'),
                    'title'    => esc_html__( 'Menu Color', 'mf' ),
                    'mode'     => 'color',
                    'default'  => '#2a2a2a',
                ),
            array(
                    'id'       => 'ad-menu-Classic-color-hover',
                    'type'     => 'color',
                    'output'    => array('#header.adclassic #nav .nav-top > li > a:hover'),
                    'title'    => esc_html__( 'Menu Hover Color', 'mf' ),
                    'mode'     => 'color',
                ),
            array(
                   'id'       => 'menu-classic-border-line',
                    'type'     => 'border',
                    'title'    => __('Menu Line', 'mf'),
                    'all'       => false,
                    'left'       => false,
                    'top'       => false,
                    'right'       => false,
                    'output'   => array('#header.adclassic #nav .nav-top > li:hover > a'),
                    'default'  => array(
 
                        'border-style'  => 'solid', 
                        'border-bottom' => '2px', 
                    )  
                ),
        ),

    ) );


     Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Post', 'mf' ),
        'id'         => 'general-post-settings',
        'icon'   => 'el el-edit',
        'fields'     => array(
            array(
                    'id' => 'post-box-author',
                    'type' => 'switch',
                    'title' => esc_html__('Post Author Box', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post author box.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),
            array(
                    'id' => 'post-meta-author',
                    'type' => 'switch',
                    'title' => esc_html__('Post Meta Author ', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post meta author name.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),
            array(
                    'id' => 'post-meta-date',
                    'type' => 'switch',
                    'title' => esc_html__('Post Meta Date', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post meta date.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),
            array(
                    'id' => 'post-meta-categories',
                    'type' => 'switch',
                    'title' => esc_html__('Post Meta Categories', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post meta categories.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),
            array(
                    'id' => 'post-meta-comments',
                    'type' => 'switch',
                    'title' => esc_html__('Post Meta Comments', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post meta comments.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),


        )
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Page Title Bar', 'mf' ),
        'id'         => 'pt-bar-general',
        'icon'   => 'el el-adjust-alt',
       
    ) );




          Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Transparent Page Title Bar', 'mf' ),
        'id'         => 'page-title-bar-tr-gl',
        'subsection' => true,
        'fields'     => array(
           array(
                'id'            => 'tr-page-title-typo',
                'type'          => 'typography',
                'output'        => array('.page-banner .heading'),
                'title'         => esc_html__( 'Page Title Font Settings', 'mf' ),
                'subtitle'      => esc_html__( 'Controls the font size for the page title heading. Enter value including CSS unit (px, em, rem), ex: 18px.', 'mf' ),
                'google'        => true,
                'line-height'   => false,
                'text-align'    => false,
                'default'       => array(
                    'font-weight'  => '300',
                    'font-family' => 'Lato',
                    'color'       => '#FFFFFF',
                    'google'      => true,
                    'font-size'   => '36px',
                
                ),
            ),
           array(
                'id'       => 'page-title-tr',
                'type'     => 'spacing',
                'output'   => array( '.page-banner .container' ),
                'mode'     => 'padding',
                'all'      => true,
                'all'      => false,
                'right'         => false,     // Disable the right
                'left'          => false,     // Disable the left
                'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
                'title'    => __( 'Page Title Padding', 'mf' ),
                'subtitle' => __( 'Controls the spacing of the page title bar on desktop. Enter value including any valid CSS unit, ex: 87px.', 'mf' ),
                'default'  => array(
                    'padding-top'    => '187px',
                    'padding-bottom' => '99px',
                )
            ),
            array(
                'id'       => 'tr-default-pg-bg',
                'type'     => 'media',
                'title'    => esc_html__( 'Default Page Title Background', 'mf' ),
                'output' => 'true',
                 'default'  => array(
                            'url'=> esc_url_raw( get_template_directory_uri().'/assets/img/default-image.jpg'),
                        ),
                'subtitle' => esc_html__('Default image in transparent page headers', 'mf'),
            ),
			 array(
                'id'       => 'woo-default-pg-bg',
                'type'     => 'media',
                'title'    => esc_html__( 'WooCommerce Transparent Header Background', 'mf' ),
                'output' => 'true',
                 'default'  => array(
                            'url'=> esc_url_raw( get_template_directory_uri().'/assets/img/woo-title.jpg'),
                        ),
                'subtitle' => esc_html__('Default image in transparent page headers', 'mf'),
            ),
           array(
                'id'       => 'breadcrumb-tr-style',
                'type'     => 'info',
                'style' => 'warning',
                'desc'    => esc_html__( 'Breadcrumb Style', 'mf' ),
            ),
           array(
                'id'       => 'tr-breadcrump-font-size',
                'type'     => 'typography',
                'output' => array('.page-banner .breadcrumbs li'),
                'title'    => esc_html__( 'Breadcrumb Font Style', 'mf' ),
                'subtitle' => esc_html__( 'Controls the font size for the page title Breadcrumb. Enter value including CSS unit (px, em, rem), ex: 18px.', 'mf' ),
                'google'   => true,
                'default'  => array(
                    'font-weight'  => '400',
                    'font-family' => 'Lato',
                    'color'       => '#bbbbbb',
                    'google'      => true,
                    'font-size'   => '13px',
                    'line-height' => '18px',
                    'text-transform' => 'uppercase',

                ),
            ),
            array(
                'id'       => 'tr-breadcrump-font-a-color',
                'type'     => 'color',
                'output'    => array('.page-banner .breadcrumbs li a'),
                'title'    => esc_html__( 'Breadcrumb Font Link Color', 'mf' ),
                'mode'     => 'color',
                'default'  => '#dbdbdb',
            ),
            )
    ) );
    




        Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Classic Page Title Bar', 'mf' ),
            'id'         => 'page-title-bar-cl-gl',
            'subsection' => true,
            'fields'     => array(
               
               
               array(
                    'id'            => 'cl-page-title-typo',
                    'type'          => 'typography',
                    'output'        => array('.page-banner.small .heading'),
                    'title'         => esc_html__( 'Page Title Font Settings', 'mf' ),
                    'subtitle'      => esc_html__( 'Controls the font size for the page title heading. Enter value including CSS unit (px, em, rem), ex: 18px.', 'mf' ),
                    'google'        => true,
                    'line-height'   => false,
                    'text-align'    => false,
                    'default'       => array(
                        'font-weight'  => '300',
                        'font-family' => 'Lato',
                        'color'       => '#727070',
                        'google'      => true,
                        'font-size'   => '36px',
                    
                    ),
                ),
               array(
                    'id'       => 'page-title-classic',
                    'type'     => 'spacing',
                    'output'   => array( '.page-banner.small .container' ),
                    'mode'     => 'padding',
                    'all'      => true,
                    'all'      => false,
                    'right'         => false,     // Disable the right
                    'left'          => false,     // Disable the left
                    'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
                    'title'    => __( 'Page Title Padding', 'mf' ),
                    'subtitle' => __( 'Controls the spacing of the page title bar on desktop. Enter value including any valid CSS unit, ex: 87px.', 'mf' ),
                    'default'  => array(
                        'padding-top'    => '60px',
                        'padding-bottom' => '60px',
                    )
                ),

               array(
                    'id'       => 'cl-default-pg-bg',
                    'type'     => 'background',
                    'title'    => esc_html__( 'Default Page Title Background', 'mf' ),
                    'subtitle' => esc_html__('Default image in transparent page headers', 'mf'),
                    'output'   => array( '.page-banner.grey' ),
                    'default'  => '#f1f1f1',
                    
                ),
               array(
                    'id'       => 'breadcrumb-classic-style',
                    'type'     => 'info',
                    'style' => 'warning',
                    'desc'    => esc_html__( 'Breadcrumb Style', 'mf' ),
                ),
               array(
                    'id'       => 'cl-breadcrump-font-size',
                    'type'     => 'typography',
                    'output' => array('.page-banner .breadcrumbs'),
                    'title'    => esc_html__( 'Breadcrumb Font Style', 'mf' ),
                    'subtitle' => esc_html__( 'Controls the font size for the page title Breadcrumb. Enter value including CSS unit (px, em, rem), ex: 18px.', 'mf' ),
                    'google'   => true,
                    'default'  => array(
                        'font-weight'  => '400',
                        'font-family' => 'Lato',
                        'color'       => '#bbbbbb',
                        'google'      => true,
                        'font-size'   => '13px',
                        'line-height' => '18px',
                        'text-transform' => 'uppercase',

                    ),
                ),
                array(
                    'id'       => 'cl-breadcrump-font-a-color',
                    'type'     => 'color',
                    'output'    => array('.page-banner.grey .breadcrumbs li a'),
                    'title'    => esc_html__( 'Breadcrumb Font Link Color', 'mf' ),
                    'mode'     => 'color',
                    'default'  => '#424141',
                ),
                )
        ) );
    
    





    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer', 'mf' ),
        'id'         => 'opt-text-footer-options',
        'icon'   => 'el el-fork',
        'fields'     => array(
		             array(
                    'id'       => 'footer-top-h',
                    'type'     => 'info',
                    'style' => 'warning',
                    'desc'    => esc_html__( 'Footer Top', 'mf' ),
                ),
		           array(
                    'id' => 'footer-top',
                    'type' => 'switch',
                    'title' => esc_html__('Footer Top', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the Footer Top area.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),
			array(
                'id'       => 'footer-top-bg',
                'type'     => 'color',
                'output'    => array( '.bg-dark-jungle' ),
                'title'    => esc_html__( 'Footer Top Backgorund Color', 'mf' ),
                'default'  => '#222222',
                'mode'     => 'background',
            ),
	        array(
                'id'       => 'footer-logo',
                'type'     => 'media',
                'title'    => esc_html__( 'Footer Top Logo', 'mf' ),
                'output' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                 'default'  => array(
                            'url'=> esc_url_raw( get_template_directory_uri().'/assets/img/mf-white.png'),
                        ),
             
            ),
            array(
                'id'             => 'footer-logo-width',
                'type'           => 'dimensions',
                'output'    => array( '.footer-top .logo img','.footer-top .logo' ),
                'units'          => array( 'em', 'px', '%' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => esc_html__( 'Footer Top Logo (Width)', 'mf' ),
                'height'         => false,
                'default'        => array(
                    'width'  => 200,
                )
            ),			
				
				
				
				


           

             array(
                    'id'       => 'footer-middle-h',
                    'type'     => 'info',
                    'style' => 'warning',
                    'desc'    => esc_html__( 'Footer Middle', 'mf' ),
                ),
				 array(
                    'id' => 'footer-middle',
                    'type' => 'switch',
                    'title' => esc_html__('Footer Middle', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the Footer Middle area.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),	
              array(
                'id'       => 'footer-middle-bg',
                'type'     => 'color',
                'output'    => array( '.bg-shark' ),
                'title'    => esc_html__( 'Footer Middle Backgorund Color', 'mf' ),
                'default'  => '#2a2a2a',
                'mode'     => 'background',
            ),				
             array(
                'id'       => 'footer-columns',
                'type'     => 'select',
                'title'    => esc_html__( 'Footer Middle Columns', 'mf' ),
                //Must provide key => value pairs for select options
                'options'  => array(
                    '1' => '1 Column',
                    '2' => '2 Column',
                    '3' => '3 Column',
                    '4' => '4 Column',
                ),
                'default'  => '4'
            ),				
				
				
				
				
				
              array(
                'id'       => 'footer-title-color',
                'type'     => 'color',
                'output'    => array( '.footer-cent h5' ),
                'title'    => esc_html__( 'Footer Middle Title Color', 'mf' ),
                'default'  => '#FFFFFF',
                'mode'     => 'color',
            ),
            array(
                'id'       => 'footer-content-color',
                'type'     => 'color',
                'output'    => array( '.footer-cent' ),
                'title'    => esc_html__( 'Footer Middle Content Color', 'mf' ),
                'default'  => '#8f8f8f',
                'mode'     => 'color',
            ),
            array(
                'id'       => 'footer-link-color',
                'type'     => 'color',
                'output'    => array( '#footer .footer-cent a' ),
                'title'    => esc_html__( 'Footer Middle Link Color', 'mf' ),
                'mode'     => 'color',
            ),
            array(
                'id'       => 'footer-link-hover',
                'type'     => 'color',
                'output'    => array( '#footer .footer-cent a:hover' ),
                'title'    => esc_html__( 'Footer Middle Link Hover', 'mf' ),
                'mode'     => 'color',
            ),
            array(
                    'id'       => 'footer-bottom-h',
                    'type'     => 'info',
                    'style' => 'warning',
                    'desc'    => esc_html__( 'Footer Bottom', 'mf' ),
                ),

				 array(
                    'id' => 'footer-bottom',
                    'type' => 'switch',
                    'title' => esc_html__('Footer Bottom', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the Footer Bottom area.', 'mf'),
                    'default' => 1,
                    'on' => 'On',
                    'off' => 'Off'
                ),

								
            array(
                'id'       => 'footer-bottom-bg',
                'type'     => 'color',
                'output'    => array( '.bg-dark-jungle-bottom' ),
                'title'    => esc_html__( 'Footer Bottom Backgorund Color', 'mf' ),
                'default'  => '#222222',
                'mode'     => 'background',
            ),				
				array(
                    'id'       => 'footer-bottom-t',
                    'type'     => 'info',
                    'style'    => 'text',
                    'desc'    => esc_html__( 'Assign Copyright text from Appearance >> Widgets.', 'mf' ),
                ),				
				array(
                    'id'       => 'footer-bottom-k',
                    'type'     => 'info',
                    'style'    => 'text',
                    'desc'    => esc_html__( 'Assign Menu from Appearance >> Menus.', 'mf' ),
                ),


            
        ),

    ) );

    
     

      // -> START Typography
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Typography', 'mf' ),
        'id'     => 'typography',
        'desc'   => esc_html__( 'For full documentation on this field, visit: ', 'mf' ) . '<a href="//docs.reduxframework.com/core/fields/typography/" target="_blank">docs.reduxframework.com/core/fields/typography/</a>',
        'icon'   => 'el el-fontsize',
    ) );

     Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General Font Settings', 'mf' ),
        'id'         => 'opt-text-general-font',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'opt-typography-body',
                'type'     => 'typography',
                'output'    => array( 'body' ),
                'title'    => esc_html__( 'Body Font', 'mf' ),
                'subtitle' => esc_html__( 'Specify the body font properties.', 'mf' ),
                'google'   => true,
                'default'  => array(
                    'color'       => '#777',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'line-height' => '26px',
                    'font-size'   => '14px',
                ),
            ),
            array(
                'id'       => 'transparent-page-title-typo',
                'type'     => 'typography',
                'output'    => array( '.page-banner .heading' ),
                'title'    => esc_html__( 'Transparent Page & Post Header Title', 'mf' ),
                'subtitle' => esc_html__( 'Specify the page & post header h1 font properties.', 'mf' ),
                'google'   => true,
                'default'  => array(
                    'color'       => '#FFFFFF',
                    'font-weight'  => '300',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '36px',
                    'line-height' => '35px'
                ),
            ),
            array(
                'id'       => 'classic-page-title-typo',
                'type'     => 'typography',
                'output'    => array( '.page-banner.small .heading' ),
                'title'    => esc_html__( 'Classic Page & Post Header Title', 'mf' ),
                'subtitle' => esc_html__( 'Specify the page & post header h1 font properties.', 'mf' ),
                'google'   => true,
                'default'  => array(
                    'color'       => '#727070',
                    'font-weight'  => '300',
                    'font-family' => 'Lato',
                    'google'      => true,
                    'font-size'   => '36px',
                    'line-height' => '40px'
                ),
            ),
             array(
                'id'       => 'post-in-title-typo',
                'type'     => 'typography',
                'output'    => array( '.blog-txt h2' ),
                'title'    => esc_html__( 'Post Title H2', 'mf' ),
                'subtitle' => esc_html__( 'Change the heading properties within the page', 'mf' ),
                'google'   => true,
                'default'  => array(
                    'color'       => '#2c2e3d',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '35px',
                    'line-height' => '40px'
                ),
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Heading Fonts', 'mf' ),
        'id'         => 'opt-text-heading-font',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'          => 'opt-typography-h1',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H1', 'mf' ),
                'output'    => array( 'h1' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                'default'     => array(
                    'color'       => '#444444',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '36px',
                    'line-height' => '36px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h2',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H2', 'mf' ),
                'output'    => array( 'h2' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                'default'     => array(
                    'color'       => '#444444',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '30px',
                    'line-height' => '30px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h3',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H3', 'mf' ),
                'output'    => array( 'h3' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                'default'     => array(
                    'color'       => '#444444',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '24px',
                    'line-height' => '24px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h4',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H4', 'mf' ),
                'output'    => array( 'h4' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                'default'     => array(
                    'color'       => '#444444',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '18px',
                    'line-height' => '18px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h5',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H5', 'mf' ),
                'output'    => array( 'h5' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                'default'     => array(
                    'color'       => '#444444',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '14px',
                    'line-height' => '14px'
                ),
            ),
            array(
                'id'          => 'opt-typography-h6',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H6', 'mf' ),
                'output'    => array( 'h6' ),
                'font-size'     => true,
                'all_styles'  => true,
                'units'       => 'px',
                'default'     => array(
                    'color'       => '#444444',
                    'font-weight'  => '400',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '12px',
                    'line-height' => '12px'
                ),
            ),



        )
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Social Media', 'mf' ),
        'id'         => 'opt-text-social-options',
        'icon'   => 'el el-globe',
        'fields'     => array(
             array(
                'id'       => 'soc_face',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Facebook', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
             array(
                'id'       => 'soc_twitter',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Twitter', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
             array(
                'id'       => 'soc_goplus',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Google Plus', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
             array(
                'id'       => 'soc_instagram',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Instagram', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
             array(
                'id'       => 'soc_linkedin',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'LinkedIn', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
              array(
                'id'       => 'soc_pinterest',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Pinterest', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
              array(
                'id'       => 'soc_youtube',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Youtube', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
              array(
                'id'       => 'soc_xing',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Xing', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
              array(
                'id'       => 'soc_android',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Android', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
              array(
                'id'       => 'soc_apple',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Apple', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => '#',
            ),
        ),

    ) );

    // Menu
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom Css & Codes', 'mf' ),
        'id'         => 'mf-custom-css',
        'icon'   => 'el el-pencil',
        'fields'     => array(
        	array(
                'id'       => 'opt-ace-editor-css',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'CSS Code', 'mf' ),
                'subtitle' => esc_html__( 'Paste your CSS code here.', 'mf' ),
                'mode'     => 'css',
                'theme'    => 'monokai',
            ),
			array(
				'id'       => 'mf-before-head',
				'type'     => 'ace_editor',
				'title'    => esc_html__( 'Code to be placed after &lt;head&gt; tag', 'mf' ),
				'subtitle' => esc_html__( 'Code here will be added just before the closing of &lt;head&gt; tag.', 'mf' ),
				'mode'     => 'text',
				'theme'    => 'eclipse',
				'options'  => array( 'minLines' => 15, 'maxLines' => 80 )
			),
			array(
				'id'       => 'mf-before-body',
				'type'     => 'ace_editor',
				'title'    => esc_html__( 'Code to be placed before closing body tag. i.e. &lt;/body&gt; tag', 'mf' ),
				'subtitle' => esc_html__( 'Code here will be added just before the closing of &lt;body&gt; tag.', 'mf' ),
				'mode'     => 'text',
				'theme'    => 'eclipse',
				'options'  => array( 'minLines' => 15, 'maxLines' => 80 )
			),
       ),

    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( '404 Error Page', 'mf' ),
        'id'         => 'error-page',
        'icon'   => 'el el-glasses',
        'fields'     => array(
            array(
                'id'       => 'search-on',
                'type'     => 'switch',
                'title'    => esc_html__( 'Search Box', 'mf' ),
                'subtitle' => esc_html__( 'Show Search Box, On or Off', 'mf' ),
                'default'  => true,
            ),
             array(
                'id'       => 'not-found-text',
                'type'     => 'text',
                'output'   => array( '' ),
                'title'    => esc_html__( 'Not Found Text', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => 'NOT FOUND',
            ),
             array(
                'id'       => 'not-found-content',
                'type'     => 'editor',
                'title'    => esc_html__( 'Not Found Text', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'default'  => 'It looks like nothing was found at this location. Maybe try one of the links below or a search?',
            ),


        ),

    ) );





    /*
     * <--- END SECTIONS
     */

 add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);
 function compiler_action($options, $css, $changed_values) {
    global $wp_filesystem;

    $filename = get_template_directory() . '/assets/css/mf-custom-style.css';


    if( empty( $wp_filesystem ) ) {
        require_once( ABSPATH .'/wp-admin/includes/file.php' );
        WP_Filesystem();
    }

    if( $wp_filesystem ) {
        $wp_filesystem->put_contents(
            $filename,
            $css,
            FS_CHMOD_FILE // predefined mode settings for WP files
        );
    }
}




