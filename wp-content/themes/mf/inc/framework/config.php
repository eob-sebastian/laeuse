<?php

// INCLUDE THIS BEFORE you load your ReduxFramework object config file.


// You may replace $redux_opt_name with a string if you wish. If you do so, change loader.php
// as well as all the instances below.
$redux_opt_name = "mf_options";


  


if (!function_exists("redux_add_metaboxes")):
    function redux_add_metaboxes($metaboxes)
    {
        global $mf_options;
    $vheader1p = get_template_directory_uri().'/assets/img/header1.png';
    $vheader2p = get_template_directory_uri().'/assets/img/header2.png';
    $vheader3p = get_template_directory_uri().'/assets/img/header3.png';
    $vheader4p = get_template_directory_uri().'/assets/img/header4.png';
    $vheader5p = get_template_directory_uri().'/assets/img/header5.png';
    $vheader6p = get_template_directory_uri().'/assets/img/header6.png';
    $vheader7p = get_template_directory_uri().'/assets/img/header7.png';
    $vheader8p = get_template_directory_uri().'/assets/img/header8.png';
    $vheader9p = get_template_directory_uri().'/assets/img/header9.png';
    $vheader10p = get_template_directory_uri().'/assets/img/header10.png';
    $vheader11p = get_template_directory_uri().'/assets/img/header11.png';
    $vheader12p = get_template_directory_uri().'/assets/img/header12.png';
    $vheader13p = get_template_directory_uri().'/assets/img/header13.png';

     $page_gl_titlebg = $mf_options['tr-default-pg-bg']['url'];
        
        // PAGE FIELDS

        // PAGE CENTER OPTIONS ----------------------------------------------------------------------------------------------------------------------------


        $boxSections[] = array(
            'title' => esc_html__('Page Header / Title', 'mf'),
            //'desc' => esc_html__('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'mf'),
            'icon' => 'el-icon-cogs',
            'fields' => array(
                array(
                'id'      => 'page-header-version',
                'type'    => 'select_image',
                'presets' => true,
                'title'   => esc_html__( 'Header Version', 'mf' ),

                'options' => array(

                    '0' => array(
                        'alt' => 'Header 1',
                        'img' => $vheader1p,
                    ),
                    '1' => array(
                        'alt' => 'Header 2',
                        'img' => $vheader2p,
                        
                    ),
                    '2' => array(
                        'alt' => 'Header 3',
                        'img' => $vheader3p,
                    ),
                    '3' => array(
                        'alt' => 'Header 4',
                        'img' => $vheader4p,
                    ),
                    '4' => array(
                        'alt' => 'Header 5',
                        'img' => $vheader5p,
                    ),
                    '5' => array(
                        'alt' => 'Header 6',
                        'img' => $vheader6p,
                    ),
                    '6' => array(
                        'alt' => 'Header 7',
                        'img' => $vheader7p,
                    ),
                    '7' => array(
                        'alt' => 'Header 8',
                        'img' => $vheader8p,
                    ),
                    '8' => array(
                        'alt' => 'Header 9',
                        'img' => $vheader9p,
                    ),
                    '9' => array(
                        'alt' => 'Header 10',
                        'img' => $vheader10p,
                    ),
                    '10' => array(
                        'alt' => 'Header 11',
                        'img' => $vheader11p,
                    ),
                    '11' => array(
                        'alt' => 'Header 12',
                        'img' => $vheader12p,
                    ),
                    '12' => array(
                        'alt' => 'Header 13',
                        'img' => $vheader13p,
                    ),
                    
                ),
             
            ),

                array(
                    'id' => 'breadcrumbs-parent',
                    'type' => 'switch',
                    'title' => esc_html__('Show Breadcrumbs ', 'mf'),
                    'subtitle' => esc_html__('', 'mf'),
                    'default' => 1,
                    'on' => 'Enabled',
                    'off' => 'Disabled'
                ),
                 array(
                'id'       => 'trans-page-set',
                'type'     => 'info',
                'style' => 'warning',
                'title'    => esc_html__( 'Transparent Page Title', 'mf' ),
            
            ),
                 array(
                'id'       => 'page-title-image',
                'type'     => 'media',
                'title'    => esc_html__( 'Page Header Image ', 'mf' ),
                'subtitle' => esc_html__( 'Only in transparent header', 'mf' ),
            ),
                array(
                'id'       => 'page-subtitle',
                'type'     => 'text',
                'title'    => esc_html__( 'Page Subtitle', 'mf' ),
                'subtitle' => esc_html__( 'Only in transparent header', 'mf' ),
            ),
                
             array(
                'id'       => 'classic-page-set',
                'type'     => 'info',
                'style' => 'warning',
                'title'    => esc_html__( 'Classic Page Title', 'mf' ),
            
            ),
                array(
                    'id' => 'page-title-show',
                    'type' => 'switch',
                    'title' => esc_html__('Show Page Title Bar ', 'mf'),
                    'subtitle' => esc_html__( 'Only in classic header. Disable for Transparent Menu over Slider.', 'mf' ),
                    'default' => 1,
                    'on' => 'Enabled',
                    'off' => 'Disabled'
                ),
               
                array(
                    'id' => 'page-classic-bg',
                    'type' => 'color',
                    'title' => esc_html__('Page Title Bar Backgorund', 'mf'),
                    'subtitle' => esc_html__('', 'mf'),
                    'required' => array(
                        'page-title-show',
                        '=',
                        '1'
                    ),
                    'default' => '#f1f1f1',
                  
                ),
                array(
                    'id' => 'page-classic-text',
                    'type' => 'color',
                    'title' => esc_html__('Page Title Bar Text Color', 'mf'),
                    'subtitle' => esc_html__('', 'mf'),
                    'required' => array('page-title-show','=','1'),
                    'default' => '#8f8f8f',
                
                ),
                
                
            )
        );

          $boxSections[] = array(
            'title' => esc_html__('Page Settings', 'mf'),
            //'desc' => esc_html__('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'mf'),
            'icon' => 'el-icon-cogs',
            'fields' => array(
               array(
                'id'       => 'page-padding',
                'type'     => 'spacing',
                // An array of CSS selectors to apply this font style to
                'mode'     => 'padding',
                'units'          => array('px'),
                'all'      => false,
                'title'    => esc_html__( 'Page Padding', 'mf' ),
                'subtitle' => esc_html__( 'Ex: 20px. Leave empty for default value.', 'mf' ),
                'default'  => array(
                    'padding-top'    => '40px',
                    'padding-right'  => '0px',
                    'padding-bottom' => '40px',
                    'padding-left'   => '0px',
                    'units'          => 'px', 
                )
            ),
               array(
                'id'       => 'page-bg-color',
                'type'     => 'color',
                'output'    => array( '' ),
                'title'    => esc_html__( 'Page Backgorund Color', 'mf' ),
                'subtitle' => esc_html__( '', 'mf' ),
                'desc'     => esc_html__('', 'mf'),
                'mode'     => 'background',
            ),   

            )
        );
        
        $boxSections[] = array(
            'title' => esc_html__('Blog', 'mf'),
            'icon' => 'el-icon-cogs',
            'post_types' => array(
                'page'
            ),
            'fields' => array(
                array(
                    'id' => 'cat_page',
                    'type' => 'select',
                    'data' => 'categories',
                    'title' => esc_html__('Select categories to show', 'mf'),
                    
                    'multi' => true,
                    'desc' => esc_html__('', 'mf'),
                    'subtitle' => esc_html__('You can make multiple selections.', 'mf')
                ),
                
            )
        );
        
        
        $metaboxes = array(); 
        $metaboxes[]   = array(
            'id' => 'demo-layout',
            'title' => esc_html__('Page Options', 'mf'),
            'post_types' => array(
                'page'
            ),
            //'page_template' => array('page-test.php'),
            //'post_format' => array('image'),
            'position' => 'normal', // normal, advanced, side
            'priority' => 'high', // high, core, default, low
            //'sidebar' => false, // enable/disable the sidebar in the normal/advanced positions
            'sections' => $boxSections
        );

        // PAGE CENTER OPTIONS END ----------------------------------------------------------------------------------------------------------------------------


        // PAGE SIDEBAR OPTIONS ----------------------------------------------------------------------------------------------------------------------------

        $boxSections   = array();
        $boxSections[] = array(
            //'title' => esc_html__('Home Settings', 'mf'),
            //'desc' => esc_html__('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'mf'),
            'icon_class' => 'icon-large',
            'icon' => 'el-icon-cogs',
            'fields' => array(
                array(
                    'title' => esc_html__('Layout', 'mf'),
                    'desc' => esc_html__('Select main content and sidebar arrangement. Choose between 1, 2 or 3 column layout.', 'mf'),
                    'id' => 'layout',
                    'default' => 0,
                    'type' => 'image_select',
                    'customizer' => array(),
                    'options' => array(
                        0 => ReduxFramework::$_url . 'assets/img/1c.png',
                        1 => ReduxFramework::$_url . 'assets/img/2cr.png',
                        2 => ReduxFramework::$_url . 'assets/img/2cl.png'
                    )
                )
            )
        );
        
        
        
        $metaboxes[] = array(
            'id' => 'demo-layout2',
            //'title' => esc_html__('Cool Options', 'mf'),
            'post_types' => array(
                'page',
                'acme_product'
            ),
            //'page_template' => array('page-test.php'),
            //'post_format' => array('image'),
            'position' => 'side', // normal, advanced, side
            'priority' => 'high', // high, core, default, low
            'sections' => $boxSections
        );

        // PAGE SIDEBAR OPTIONS END ----------------------------------------------------------------------------------------------------------------------------


        $post_options   = array();
        $post_options[] = array(
            'icon_class' => 'icon-large',
            'icon' => 'el-icon-cogs',
            'fields' => array(
                array(
                    'title' => esc_html__('Layout', 'mf'),
                    'desc' => esc_html__('Select main content and sidebar arrangement. Choose between 1, 2 or 3 column layout.', 'mf'),
                    'id' => 'layout-post',
                    'default' => 1,
                    'type' => 'image_select',
                    'customizer' => array(),
                    'options' => array(
                        0 => ReduxFramework::$_url . 'assets/img/1c.png',
                        1 => ReduxFramework::$_url . 'assets/img/2cr.png',
                        2 => ReduxFramework::$_url . 'assets/img/2cl.png'
                    )
                )
            )
        );

           $metaboxes[] = array(
            'id' => 'demo-layout2',
            //'title' => esc_html__('Cool Options', 'mf'),
            'post_types' => array('post'),
            //'page_template' => array('page-test.php'),
            //'post_format' => array('image'),
            'position' => 'side', // normal, advanced, side
            'priority' => 'high', // high, core, default, low
            'sections' => $post_options
        );












        // PRODUCT SIDEBAR OPTIONS ----------------------------------------------------------------------------------------------------------------------------
        
        $productSections   = array();
        $productSections[] = array(
            'icon_class' => 'icon-large',
            'icon' => 'el-icon-cogs',
            'fields' => array(
                array(
                    'title' => esc_html__('Layout', 'mf'),
                    'id' => 'product_layout',
                    'default' => 0,
                    'type' => 'image_select',
                    'customizer' => array(),
                    'options' => array(
                        0 => ReduxFramework::$_url . 'assets/img/1c.png',
                        1 => ReduxFramework::$_url . 'assets/img/2cr.png',
                        2 => ReduxFramework::$_url . 'assets/img/2cl.png'
                        
                    )
                )
            )
        );

         $productSections[] = array(
            //'title'         => esc_html__('Post Settings', 'mf'),
            'icon_class' => 'icon-large',
            'icon' => 'el-icon-website',
            'fields' => array(
                array(
                    'id' => 'product_sidebar',
                    'title' => esc_html__('Select Sidebar', 'mf'),
                    'desc' => 'Please select the sidebar you would like to display on this page. Note: You must first create the sidebar under Appearance > Widgets.',
                    'type' => 'select',
                    'data' => 'sidebars',
                    'default' => 'default_sidebar'
                )
            )
        );
        
        
        
        $metaboxes[] = array(
            'id' => 'product_layout',
            //'title' => esc_html__('Cool Options', 'mf'),
            'post_types' => array(
                'product'
            ),
            //'page_template' => array('page-test.php'),
            //'post_format' => array('image'),
            'position' => 'side', // normal, advanced, side
            'priority' => 'high', // high, core, default, low
            'sections' => $productSections
        );

        // PRODUCT SIDEBAR OPTIONS END ----------------------------------------------------------------------------------------------------------------------------

        
        
        // CUSTOM SIDEBAR START ----------------------------------------------------------------------------------------------------------------------------    
        
        $sidebarmeta[] = array(
            //'title'         => esc_html__('Post Settings', 'mf'),
            'icon_class' => 'icon-large',
            'icon' => 'el-icon-website',
            'fields' => array(
                array(
                    'id' => 'mf_sidebar',
                    'title' => esc_html__('Select Sidebar', 'mf'),
                    'desc' => 'Please select the sidebar you would like to display on this page. Note: You must first create the sidebar under Appearance > Widgets.',
                    'type' => 'select',
                    'data' => 'sidebars',
                    'default' => 'default_sidebar'
                )
            )
        );
        
        $metaboxes[] = array(
            'id' => 'demo-layout2222',
            'title' => esc_html__('Sidebar', 'mf'),
            'post_types' => array(
                'page',
                'post',
                'acme_product'
            ),
            //'page_template' => array('page-test.php'),
            //'post_format' => array('image'),
            'position' => 'side', // normal, advanced, side
            'priority' => 'high', // high, core, default, low
            'default' => 'default_sidebar',
            'sections' => $sidebarmeta
        );

        // CUSTOM SIDEBAR END ----------------------------------------------------------------------------------------------------------------------------    
  
    $post_options = array();
    $post_options[] = array(
            'title' => esc_html__('Post Header / Title', 'mf'),
            //'desc' => esc_html__('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'mf'),
            'icon' => 'el-icon-cogs',
            'fields' => array(
                 array(
                    'id' => 'post-image',
                    'type' => 'switch',
                    'title' => esc_html__('Featured Images ', 'mf'),
                    'subtitle' => esc_html__('', 'mf'),
                    'default' => 1,
                    'on' => 'Enabled',
                    'off' => 'Disabled'
                ),
                array(
                    'id' => 'post-breadcrumbs-parent',
                    'type' => 'switch',
                    'title' => esc_html__('Show Breadcrumbs ', 'mf'),
                    'subtitle' => esc_html__('', 'mf'),
                    'default' => 1,
                    'on' => 'Enabled',
                    'off' => 'Disabled'
                ),
                 array(
                'id'       => 'post-trans-page-set',
                'type'     => 'info',
                'style' => 'warning',
                'title'    => esc_html__( 'Transparent Post Title', 'mf' ),
            
            ),
                 array(
                'id'       => 'post-title-image',
                'type'     => 'media',
                'title'    => esc_html__( 'Post Header Image ', 'mf' ),
                'subtitle' => esc_html__( 'Only in transparent header', 'mf' ),
            ),
                array(
                'id'       => 'post-subtitle',
                'type'     => 'text',
                'title'    => esc_html__( 'Post Subtitle', 'mf' ),
                'subtitle' => esc_html__( 'Only in transparent header', 'mf' ),
            ),
                
             array(
                'id'       => 'classic-post-set',
                'type'     => 'info',
                'style' => 'warning',
                'title'    => esc_html__( 'Classic Post Title', 'mf' ),
            
            ),
                array(
                    'id' => 'post-title-show',
                    'type' => 'switch',
                    'title' => esc_html__('Show Post Title Bar ', 'mf'),
                    'subtitle' => esc_html__( 'Only in classic header', 'mf' ),
                    'default' => 1,
                    'on' => 'Enabled',
                    'off' => 'Disabled'
                ),
               
                array(
                    'id' => 'post-classic-bg',
                    'type' => 'color',
                    'title' => esc_html__('Post Title Bar Backgorund', 'mf'),
                    'subtitle' => esc_html__('', 'mf'),
                    'required' => array(
                        'page-title-show',
                        '=',
                        '1'
                    ),
                    'default' => '#f1f1f1',
                  
                ),
                array(
                    'id' => 'post-classic-text',
                    'type' => 'color',
                    'title' => esc_html__('Post Title Bar Text Color', 'mf'),
                    'subtitle' => esc_html__('', 'mf'),
                    'required' => array('page-title-show','=','1'),
                    'default' => '#8f8f8f',
                
                ),
                
                
            )
        );
     $post_options[] = array(
            'title' => esc_html__('Post Settings', 'mf'),
            //'desc' => esc_html__('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'mf'),
            'icon' => 'el-icon-cogs',
            'fields' => array(

               array(
                'id'       => 'post-padding',
                'type'     => 'spacing',
                // An array of CSS selectors to apply this font style to
                'units'          => array('px'),
                'mode'     => 'padding',
                'all'      => false,
                'title'    => esc_html__( 'Post Padding', 'mf' ),
                'subtitle' => esc_html__( 'Ex: 20px. Leave empty for default value.', 'mf' ),
                'default'  => array(
                    'padding-top'    => '40px',
                    'padding-right'  => '0px',
                    'padding-bottom' => '40px',
                    'padding-left'   => '0px',
                    'units'          => 'px', 
                )
            ),


            )
        );

     $post_options[] = array(
            'title' => esc_html__('Post Meta', 'mf'),
            'icon' => 'el el-indent-left',
            'fields' => array(
            array(
                    'id' => 'si-post-meta-author',  
                    'type' => 'select',
                    'title' => esc_html__('Post Meta Author ', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post meta author name.', 'mf'),
                    'options'  => array(
                        '0' => 'Default',
                        '1' => 'Show',
                        '2' => 'Hide',
                                        ),
                    'default'  => 0,
                ),
            array(
                    'id' => 'si-post-meta-date',
                    'type' => 'select',
                    'title' => esc_html__('Post Meta Date', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post meta date.', 'mf'),
                    'options'  => array(
                        '0' => 'Default',
                        '1' => 'Show',
                        '2' => 'Hide',
                                        ),
                    'default'  => 0,
                ),
            array(
                    'id' => 'si-post-meta-categories',
                    'type' => 'select',
                    'title' => esc_html__('Post Meta Categories', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post meta categories.', 'mf'),
                    'options'  => array(
                        '0' => 'Default',
                        '1' => 'Show',
                        '2' => 'Hide',
                                        ),
                    'default'  => 0,
                ),
            array(
                    'id' => 'si-post-meta-comments',
                    'type' => 'select',
                    'title' => esc_html__('Post Meta Comments', 'mf'),
                    'subtitle' => esc_html__('Turn on to display the post meta comments.', 'mf'),
                    'options'  => array(
                        '0' => 'Default',
                        '1' => 'Show',
                        '2' => 'Hide',
                                        ),
                    'default'  => 0,
                ),
               

            )
        );

    

     

    $metaboxes[] = array(
        'id'            => 'post-options',
        'title'         => esc_html__( 'Post Options', 'mf' ),
        'post_types'    => array( 'post', 'demo_metaboxes' ),
        //'page_template' => array('page-test.php'),
        //'post_format' => array('image'),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $post_options,
    );

 




        
        // Kind of overkill, but ahh well.  ;)
        //$metaboxes = apply_filters( 'your_custom_redux_metabox_filter_here', $metaboxes );
        
        return $metaboxes;
    }
    add_action('redux/metaboxes/' . $redux_opt_name . '/boxes', 'redux_add_metaboxes');
endif;





// The loader will load all of the extensions automatically based on your $redux_opt_name

require_once get_template_directory() . '/inc/framework/loader.php';


/************************************************************************
* Example functions/filters
*************************************************************************/
if ( !function_exists( 'wbc_after_content_import' ) ) {

    /**
     * Function/action ran after import of content.xml file
     *
     * @param (array) $demo_active_import       Example below
     * [wbc-import-1] => Array
     *      (
     *            [directory] => current demo data folder name
     *            [content_file] => content.xml
     *            [image] => screen-image.png
     *            [theme_options] => theme-options.txt
     *            [widgets] => widgets.json
     *            [imported] => imported
     *        )
     * @param (string) $demo_data_directory_path path to current demo folder being imported.
     *
     */

    function wbc_after_content_import( $demo_active_import , $demo_data_directory_path ) {
        //Do something
    }

    // Uncomment the below
    // add_action( 'wbc_importer_after_content_import', 'wbc_after_content_import', 10, 2 );
}

if ( !function_exists( 'wbc_filter_title' ) ) {

    /**
     * Filter for changing demo title in options panel so it's not folder name.
     *
     * @param [string] $title name of demo data folder
     *
     * @return [string] return title for demo name.
     */

    function wbc_filter_title( $title ) {
        return trim( ucfirst( str_replace( "-", " ", $title ) ) );
    }

    // Uncomment the below
    // add_filter( 'wbc_importer_directory_title', 'wbc_filter_title', 10 );
}

if ( !function_exists( 'wbc_importer_description_text' ) ) {

    /**
     * Filter for changing importer description info in options panel
     * when not setting in Redux config file.
     *
     * @param [string] $title description above demos
     *
     * @return [string] return.
     */

    function wbc_importer_description_text( $description ) {

        $message = '<p>'. esc_html__( 'Best if used on new WordPress install.', 'mf' ) .'</p>';
        $message .= '<p>'. esc_html__( 'Images are for demo purpose only.', 'mf' ) .'</p>';

        return $message;
    }

    // Uncomment the below
    // add_filter( 'wbc_importer_description', 'wbc_importer_description_text', 10 );
}

if ( !function_exists( 'wbc_importer_label_text' ) ) {

    /**
     * Filter for changing importer label/tab for redux section in options panel
     * when not setting in Redux config file.
     *
     * @param [string] $title label above demos
     *
     * @return [string] return no html
     */

    function wbc_importer_label_text( $label_text ) {

        $label_text = 'WBC Importer';

        return $label_text;
    }

    // Uncomment the below
    // add_filter( 'wbc_importer_label', 'wbc_importer_label_text', 10 );
}

if ( !function_exists( 'wbc_change_demo_directory_path' ) ) {

    /**
     * Change the path to the directory that contains demo data folders.
     *
     * @param [string] $demo_directory_path
     *
     * @return [string]
     */

    function wbc_change_demo_directory_path( $demo_directory_path ) {

        //$demo_directory_path = get_template_directory().'/demo-data/';

        return $demo_directory_path;

    }

    // Uncomment the below
    // add_filter('wbc_importer_dir_path', 'wbc_change_demo_directory_path' );
}

if ( !function_exists( 'wbc_importer_before_widget' ) ) {

    /**
     * Function/action ran before widgets get imported
     *
     * @param (array) $demo_active_import       Example below
     * [wbc-import-1] => Array
     *      (
     *            [directory] => current demo data folder name
     *            [content_file] => content.xml
     *            [image] => screen-image.png
     *            [theme_options] => theme-options.txt
     *            [widgets] => widgets.json
     *            [imported] => imported
     *        )
     * @param (string) $demo_data_directory_path path to current demo folder being imported.
     *
     * @return nothing
     */

    function wbc_importer_before_widget( $demo_active_import , $demo_data_directory_path ) {

        //Do Something

    }

    // Uncomment the below
     add_action('wbc_importer_before_widget_import', 'wbc_importer_before_widget', 10, 2 );
}

if ( !function_exists( 'wbc_after_theme_options' ) ) {

    /**
     * Function/action ran after theme options set
     *
     * @param (array) $demo_active_import       Example below
     * [wbc-import-1] => Array
     *      (
     *            [directory] => current demo data folder name
     *            [content_file] => content.xml
     *            [image] => screen-image.png
     *            [theme_options] => theme-options.txt
     *            [widgets] => widgets.json
     *            [imported] => imported
     *        )
     * @param (string) $demo_data_directory_path path to current demo folder being imported.
     *
     * @return nothing
     */

    function wbc_after_theme_options( $demo_active_import , $demo_data_directory_path ) {

        //Do Something

    }

    // Uncomment the below
     add_action('wbc_importer_after_theme_options_import', 'wbc_after_theme_options', 10, 2 );
}


/************************************************************************
* Extended Example:
* Way to set menu, import revolution slider, and set home page.
*************************************************************************/

if ( !function_exists( 'wbc_extended_example' ) ) {
    function wbc_extended_example( $demo_active_import , $demo_directory_path ) {

        reset( $demo_active_import );
        $current_key = key( $demo_active_import );

        /************************************************************************
        * Import slider(s) for the current demo being imported
        *************************************************************************/

        if ( class_exists( 'RevSlider' ) ) {

            //If it's demo3 or demo5
            $wbc_sliders_array = array(
                'Main' => 'business-1.zip',  //Set slider zip name


            );

            if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_sliders_array ) ) {
                $wbc_slider_import = $wbc_sliders_array[$demo_active_import[$current_key]['directory']];

                if ( file_exists( $demo_directory_path.$wbc_slider_import ) ) {
                    $slider = new RevSlider();
                    $slider->importSliderFromPost( true, true, $demo_directory_path.$wbc_slider_import );
                }
            }
        }

       /************************************************************************
        * Setting Menus
        *************************************************************************/

        // If it's demo1 - demo6
        $wbc_menu_array = array( 'Main' );


        if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_array ) ) {
            $main_navi  = get_term_by( 'name', 'Main', 'nav_menu' );
            $top_navi  = get_term_by( 'name', 'Top', 'nav_menu' );
            $footer_navi = get_term_by( 'name', 'Footer', 'nav_menu' );
            $left_navi = get_term_by( 'name', 'Left', 'nav_menu' );			

            if ( isset( $main_navi->term_id ) && isset( $top_navi->term_id ) && isset( $footer_navi->term_id ) ) {
                set_theme_mod( 'nav_menu_locations', array(
                        'main-menu' => $main_navi->term_id,
                        'top-menu' => $top_navi->term_id,
                        'footer-menu'  => $footer_navi->term_id,
						'left-menu'  => $left_navi->term_id,
                    )
                );
            }
        }

        /************************************************************************
        * Set HomePage
        *************************************************************************/

        // array of demos/homepages to check/select from
        $wbc_home_pages = array(
            'Main' => 'Business 1',
        );

        if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
            $page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
            if ( isset( $page->ID ) ) {
                update_option( 'page_on_front', $page->ID );
                update_option( 'show_on_front', 'page' );
            }
        }

    }


    // Uncomment the below
    add_action( 'wbc_importer_after_content_import', 'wbc_extended_example', 10, 2 );
}

