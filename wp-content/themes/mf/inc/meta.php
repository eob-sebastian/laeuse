<?php global $mf_options; 

// Genel
$post_meta_author = $mf_options['post-meta-author'];
$post_meta_date = $mf_options['post-meta-date'];
$post_meta_categories = $mf_options['post-meta-categories'];
$post_meta_comments = $mf_options['post-meta-comments'];


//Yerel
$si_meta_author = $mf_options['si-post-meta-author'];
$si_meta_date = $mf_options['si-post-meta-date'];
$si_meta_categories = $mf_options['si-post-meta-categories'];
$si_meta_comments = $mf_options['si-post-meta-comments'];
?>


<?php if ($post_meta_author == '0' && $post_meta_date == '0' && $post_meta_categories == '0' && $post_meta_comments == '0' ) { ?>

<?php if ($si_meta_author == '1' || $si_meta_author == '1' || $si_meta_categories == '1' || $si_meta_comments == '1' ) { ?>
<div class="entry-meta">
  <ul>
  <!-- Date -->
   <?php if ($post_meta_date == '1') { ?>
    <li class="meta-date">
      <i class="fa fa-calendar"></i>
      <?php the_time(get_option('date_format')); ?>
    </li>
    <?php }else { ?>
    <?php if ($si_meta_date == '1') { ?>
    <li class="meta-date">
      <i class="fa fa-calendar"></i>
      <?php the_time(get_option('date_format')); ?>
    </li>
     <?php }else {} ?>
    <?php  } ?>

    <!-- Author -->
    <?php if ($post_meta_author == '1') { ?>
    <li class="meta-author">
      <i class="fa fa-user"></i>
      <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta( 'ID' ))); ?>" title="<?php esc_html__('View all posts by', 'mf'); ?> <?php the_author(); ?>">
        <?php the_author(); ?>
      </a>
    </li>
    <?php }else { ?>
    <?php if ($si_meta_author == '1') { ?>
     <li class="meta-author">
      <i class="fa fa-user"></i>
      <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta( 'ID' ))); ?>" title="<?php esc_html__('View all posts by', 'mf'); ?> <?php the_author(); ?>">
        <?php the_author(); ?>
      </a>
    </li>
    <?php }else {} ?>
    <?php } ?>

    <!-- Comments -->
    <?php if ($post_meta_comments == '1') { ?>
    <?php if ( comments_open() ) : ?>
    <li class="meta-comment">
      <i class="fa fa-commenting"></i>
      <?php comments_popup_link( esc_html__('No Comments', 'mf'), esc_html__('1 Comment', 'mf'), esc_html__('% Comments', 'mf'), 'comments-link', ''); ?>
    </li>
    <?php endif; ?>
    <?php }else { ?>
     <?php if ($si_meta_comments == '1') { ?>
    <li class="meta-comment">
      <i class="fa fa-commenting"></i>
      <?php comments_popup_link( esc_html__('No Comments', 'mf'), esc_html__('1 Comment', 'mf'), esc_html__('% Comments', 'mf'), 'comments-link', ''); ?>
    </li>
    <?php }else {} ?>
     <?php } ?>

    <!-- Categories -->
    <?php if ($post_meta_categories == '1') { ?>
    <li class="meta-category">
      <i class="fa fa-bars"></i>
      <?php the_category(', '); ?>
    </li>
    <?php }else { ?>
    <?php if ($si_meta_categories == '1') { ?>
    <li class="meta-category">
      <i class="fa fa-bars"></i>
      <?php the_category(', '); ?>
    </li>
    <?php }else {} ?>
    <?php } ?>
  </ul>
</div>
<?php }else {} ?>
<?php }else { ?>

<?php if ($si_meta_author == '2' && $si_meta_author == '2' && $si_meta_categories == '2' && $si_meta_comments == '2' ) { }else { ?>
<div class="entry-meta">
  <ul>
  <!-- Date -->
   <?php if ($post_meta_date == '1') { ?>
   <?php if ($si_meta_date == '1' || $si_meta_date == '0' ) { ?>
    <li class="meta-date">
      <i class="fa fa-calendar"></i>
      <?php the_time(get_option('date_format')); ?>
    </li>
    <?php }else {} ?>
    <?php }else { ?>
    <?php if ($si_meta_date == '1') { ?>
     <li class="meta-date">
      <i class="fa fa-calendar"></i>
      <?php the_time(get_option('date_format')); ?>
    </li>
    <?php }else {} ?>
    <?php  } ?>

    <!-- Author -->
    <?php if ($post_meta_author == '1') { ?>
     <?php if ($si_meta_author == '1' || $si_meta_author == '0' ) { ?>
    <li class="meta-author">
      <i class="fa fa-user"></i>
      <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta( 'ID' ))); ?>" title="<?php esc_html__('View all posts by', 'mf'); ?> <?php the_author(); ?>">
        <?php the_author(); ?>
      </a>
    </li>
    <?php }else {} ?>
    <?php }else { ?>
    <?php if ($si_meta_author == '1') { ?>
     <li class="meta-author">
      <i class="fa fa-user"></i>
      <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta( 'ID' ))); ?>" title="<?php esc_html__('View all posts by', 'mf'); ?> <?php the_author(); ?>">
        <?php the_author(); ?>
      </a>
    </li>
    <?php }else {} ?>
    <?php } ?>

    <!-- Comments -->
    <?php if ($post_meta_comments == '1') { ?>
    <?php if ($si_meta_comments == '1' || $si_meta_comments == '0' ) { ?>
    <?php if ( comments_open() ) : ?>
    <li class="meta-comment">
      <i class="fa fa-commenting"></i>
      <?php comments_popup_link( esc_html__('No Comments', 'mf'), esc_html__('1 Comment', 'mf'), esc_html__('% Comments', 'mf'), 'comments-link', ''); ?>
    </li>
    <?php endif; ?>
    <?php }else {} ?>
    <?php }else { ?>
    <?php if ($si_meta_comments == '1') { ?>
    <li class="meta-comment">
      <i class="fa fa-commenting"></i>
      <?php comments_popup_link( esc_html__('No Comments', 'mf'), esc_html__('1 Comment', 'mf'), esc_html__('% Comments', 'mf'), 'comments-link', ''); ?>
    </li>
    <?php }else {} ?>
     <?php } ?>

    <!-- Categories -->
    <?php if ($post_meta_categories == '1') { ?>
    <?php if ($si_meta_categories == '1' || $si_meta_categories == '0' ) { ?>
    <li class="meta-category">
      <i class="fa fa-bars"></i>
      <?php the_category(', '); ?>
    </li>
    <?php }else {} ?>
    <?php }else { ?>
    <?php if ($si_meta_categories == '1') { ?>
    <li class="meta-category">
      <i class="fa fa-bars"></i>
      <?php the_category(', '); ?>
    </li>
    <?php }else {} ?>
     <?php } ?>
  </ul>
</div>
<?php } ?>
<?php } ?>