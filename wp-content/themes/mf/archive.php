<?php
/**
* The template for displaying archive pages.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package MF Theme
*/
get_header(); 

if (isset($mf_options['page-title-image']['url'])) {     $page_images = $mf_options['page-title-image']['url'];    }else{     $page_images = '' ;}
if (isset($mf_options['tr-default-pg-bg']['url'])) {     $glpageimage = $mf_options['tr-default-pg-bg']['url'];    }else{     $glpageimage = esc_url_raw( get_template_directory_uri().'/assets/img/default-image.jpg'); ;}
?>
 <div class="page-banner">
          <div class="stretch">
          <?php  if ($page_images == '') { ?>
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($glpageimage); ?>" >
           <?php }else {   ?> 
              <img alt="<?php the_title(); ?>" src="<?php echo esc_url($page_images); ?>" >
           <?php } ?> 
          </div>
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="holder">
                  <h1 class="heading text-capitalize"> <?php the_archive_title () ?></h1>
                 
                </div>
                <ul class="breadcrumbs list-inline">
                  <?php mf_breadcrumbs(); ?>
                </ul>
              </div>
            </div>
          </div>
        </div> 
<!-- C-LAYOUT-BREADCRUMBS -->

<!-- END C-LAYOUT-BREADCRUMBS -->
<div class="content-main">
  <div id="primary" class="site-content">
    <div id="content" role="main">  
    <?php require get_template_directory() . '/template-parts/mf-archive.php'; ?>
</div><!-- #content -->
  </div><!-- #primary -->
</div>

<?php get_footer(); ?>